# frozen_string_literal: true

# User - Mobile
User.create(email: 'test-user@mailinator.com',
            name: 'User Mobile 1',
            password: 'pass123',
            password_confirmation: 'pass123',
            phone_number: '08111111',
            user_profile_attributes: { account_number: '123456',
                                       bussiness_name: 'Cookies 1',
                                       bussiness_description: 'Cookies and Desert',
                                       bussiness_address: 'Jalan 1 Bandung',
                                       longitude: 107.619125,
                                       latitude: -6.917464 })

User.create(email: 'test-user-2@mailinator.com',
            name: 'User Mobile 2',
            password: 'pass123',
            password_confirmation: 'pass123',
            phone_number: '08111112',
            user_profile_attributes: { account_number: '123457',
                                       bussiness_name: 'Clothes 2',
                                       bussiness_description: 'Clothings',
                                       bussiness_address: 'Jalan 2 Bandung',
                                       longitude: 107.619125,
                                       latitude: -6.917464 })

User.create(email: 'test-user-3@mailinator.com',
            name: 'User Mobile 3',
            password: 'pass123',
            password_confirmation: 'pass123',
            phone_number: '08111113',
            user_profile_attributes: { account_number: '123458',
                                       bussiness_name: 'Food 3',
                                       bussiness_description: 'Food and Veggies',
                                       bussiness_address: 'alan 3 Bandung',
                                       longitude: 107.619125,
                                       latitude: -6.917464 })

# User - Admin
User.create(email: 'test-user-4@mailinator.com',
            name: 'User Admin 1',
            password: 'pass123',
            password_confirmation: 'pass123',
            phone_number: '08111114',
            user_type: 'admin')

User.create(email: 'test-user-5@mailinator.com',
            name: 'User Admin 2',
            password: 'pass123',
            password_confirmation: 'pass123',
            phone_number: '08111115',
            user_type: 'admin')

User.create(email: 'test-user-6@mailinator.com',
            name: 'User Admin 3',
            password: 'pass123',
            password_confirmation: 'pass123',
            phone_number: '08111116',
            user_type: 'admin')
