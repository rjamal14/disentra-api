class RenameObjectTypeIntoPermissionTypeOnPermissions < ActiveRecord::Migration[6.0]
  def change
    rename_column :permissions, :object_type, :permission_type
  end
end
