class AddWhatsappNumberUserProfile < ActiveRecord::Migration[6.0]
  def change
    add_column :user_profiles, :whatsapp_number, :string
  end
end
