class ChangeIntegerLimitInVillage < ActiveRecord::Migration[6.0]
  def change
    change_column :villages, :id, :integer, limit: 8
  end
end
