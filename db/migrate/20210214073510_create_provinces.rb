class CreateProvinces < ActiveRecord::Migration[6.0]
  def change
    create_table :provinces do |t|
      t.string   :name
      t.integer  :created_by_id
      t.integer  :updated_by_id
      t.integer  :deleted_by_id
      t.datetime :deleted_at
      t.timestamps
    end
  end
end
