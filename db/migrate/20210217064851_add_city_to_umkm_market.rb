class AddCityToUmkmMarket < ActiveRecord::Migration[6.0]
  def change
    add_reference :umkm_markets, :city, index: true
  end
end
