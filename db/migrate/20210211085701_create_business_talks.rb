class CreateBusinessTalks < ActiveRecord::Migration[6.0]
  def change
    create_table :business_talks do |t|
      t.string :talk_type
      t.string :link
      t.string :title
      t.string :theme
      t.date :event_on
      t.string :start_time
      t.string :end_time
      t.string :image
      t.boolean :health_protocol
      t.integer :created_by_id
      t.integer :updated_by_id
      t.integer :deleted_by_id
      t.datetime :deleted_at
      t.timestamps
    end
  end
end
