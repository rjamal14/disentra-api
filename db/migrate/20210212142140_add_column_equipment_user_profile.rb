class AddColumnEquipmentUserProfile < ActiveRecord::Migration[6.0]
  def change
    add_column :user_profiles, :nip, :string
    add_reference :user_profiles, :job_position_id, index: true
    add_reference :user_profiles, :branch_office_id, index: true
  end
end
