class AddStatusToBusinessTalks < ActiveRecord::Migration[6.0]
  def change
    add_column :business_talks, :status, :string
  end
end
