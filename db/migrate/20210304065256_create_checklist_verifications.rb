class CreateChecklistVerifications < ActiveRecord::Migration[6.0]
  def change
    create_table :checklist_verifications do |t|
      t.string   :name
      t.timestamps
    end
  end
end
