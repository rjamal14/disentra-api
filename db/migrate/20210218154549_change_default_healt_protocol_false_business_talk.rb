class ChangeDefaultHealtProtocolFalseBusinessTalk < ActiveRecord::Migration[6.0]
  def change
    change_column :business_talks, :health_protocol, :boolean, default: false
  end
end
