class AddDescriptionBusinessTalks < ActiveRecord::Migration[6.0]
  def change
    add_column :business_talks, :description, :text
  end
end
