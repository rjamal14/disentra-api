class RemoveDescriptionOnJobPositions < ActiveRecord::Migration[6.0]
  def change
    remove_column :job_positions, :description
  end
end
