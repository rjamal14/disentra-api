class CreateBusinessPhoto < ActiveRecord::Migration[6.0]
  def change
    create_table :business_photos do |t|
      t.references :user, index: true
      t.string :image
      t.timestamps
    end
  end
end
