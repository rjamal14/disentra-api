class CreateNews < ActiveRecord::Migration[6.0]
  def change
    create_table :news do |t|
      t.string   :title
      t.text     :body
      t.string   :image
      t.datetime :published_at
      t.timestamps
    end
  end
end
