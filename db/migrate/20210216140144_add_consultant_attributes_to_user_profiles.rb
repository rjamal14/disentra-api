class AddConsultantAttributesToUserProfiles < ActiveRecord::Migration[6.0]
  def change
    add_column :user_profiles, :account_type, :string
    add_column :user_profiles, :consultant_profession, :string
    add_column :user_profiles, :account_description, :text
  end
end
