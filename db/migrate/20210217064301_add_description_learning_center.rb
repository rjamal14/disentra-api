class AddDescriptionLearningCenter < ActiveRecord::Migration[6.0]
  def change
    add_column :learning_centers, :description, :text
  end
end
