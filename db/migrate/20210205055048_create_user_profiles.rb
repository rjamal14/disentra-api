class CreateUserProfiles < ActiveRecord::Migration[6.0]
  def change
    create_table :user_profiles do |t|
      t.integer  :user_id
      t.string   :account_number
      t.string   :identity_photo
      t.string   :bussiness_name
      t.text     :bussiness_description
      t.text     :bussiness_address
      t.float    :longitude
      t.float    :latitude
      t.timestamps
    end
  end
end
