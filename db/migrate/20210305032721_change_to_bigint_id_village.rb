class ChangeToBigintIdVillage < ActiveRecord::Migration[6.0]
  def change
    change_column :villages, :id, :bigint
  end
end
