class ChangeDefaultThumbToFalseUmkmMarketImages < ActiveRecord::Migration[6.0]
  def change
    change_column :umkm_market_images, :is_thumbnail, :boolean, default: false
  end
end
