class CreatePermissions < ActiveRecord::Migration[6.0]
  def change
    create_table :permissions do |t|
      t.string :object_type
      t.string :action_name
      t.text   :description
      t.timestamps
    end
  end
end
