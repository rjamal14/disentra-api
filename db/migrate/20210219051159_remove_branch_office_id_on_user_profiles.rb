class RemoveBranchOfficeIdOnUserProfiles < ActiveRecord::Migration[6.0]
  def change
    remove_column :user_profiles, :branch_office_id
  end
end
