class CreateLearningCenters < ActiveRecord::Migration[6.0]
  def change
    create_table :learning_centers do |t|
      t.string :content_type
      t.string :link
      t.string :title
      t.string :image
      t.string :author_name
      t.integer :created_by_id
      t.integer :updated_by_id
      t.integer :deleted_by_id
      t.datetime :deleted_at
      t.timestamps
    end
  end
end
