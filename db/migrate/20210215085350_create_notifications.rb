class CreateNotifications < ActiveRecord::Migration[6.0]
  def change
    create_table :notifications do |t|
      t.integer :user_id
      t.string :title
      t.text :message
      t.string :action
      t.boolean :has_read
      t.references :event, polymorphic: true, index: true
      t.timestamps
    end
  end
end
