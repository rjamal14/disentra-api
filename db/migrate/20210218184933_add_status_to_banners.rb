class AddStatusToBanners < ActiveRecord::Migration[6.0]
  def change
    add_column :banners, :status, :string
  end
end
