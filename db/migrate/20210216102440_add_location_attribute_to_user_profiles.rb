class AddLocationAttributeToUserProfiles < ActiveRecord::Migration[6.0]
  def change
    add_column :user_profiles, :province_id, :integer
    add_column :user_profiles, :city_id, :integer
    add_column :user_profiles, :district_id, :integer
    add_column :user_profiles, :village_id, :integer
  end
end
