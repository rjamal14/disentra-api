class CreateBusinessVerifications < ActiveRecord::Migration[6.0]
  def change
    create_table :business_verifications do |t|
      t.references :user
      t.references :checklist_verification
      t.text       :note
      t.timestamps
    end
  end
end
