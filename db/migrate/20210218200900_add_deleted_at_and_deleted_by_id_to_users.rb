class AddDeletedAtAndDeletedByIdToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :deleted_by_id, :integer
    add_column :users, :deleted_at, :datetime
  end
end
