class AddSpeakerToBusinessTalks < ActiveRecord::Migration[6.0]
  def change
    add_column :business_talks, :speaker, :string
  end
end
