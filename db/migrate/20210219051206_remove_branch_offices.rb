class RemoveBranchOffices < ActiveRecord::Migration[6.0]
  def change
    drop_table :branch_offices
  end
end
