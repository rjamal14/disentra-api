class CreateUmkmMarkets < ActiveRecord::Migration[6.0]
  def change
    create_table :umkm_markets do |t|
      t.string :name
      t.text :description
      t.references :business_field, index: true
      t.string :link
      t.string :address
      t.float :latitude
      t.float :longitude
      t.integer  :created_by_id
      t.integer  :updated_by_id
      t.integer  :deleted_by_id
      t.datetime :deleted_at
      t.timestamps
    end
  end
end
