class CreateContentView < ActiveRecord::Migration[6.0]
  def change
    create_table :content_views do |t|
      t.references :user, index: true
      t.references :learning_center, index: true
      t.timestamps
    end
  end
end
