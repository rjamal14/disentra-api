class AddStatusToLearningCenter < ActiveRecord::Migration[6.0]
  def change
    add_column :learning_centers, :status, :string
  end
end
