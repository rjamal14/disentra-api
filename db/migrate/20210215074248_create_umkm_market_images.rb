class CreateUmkmMarketImages < ActiveRecord::Migration[6.0]
  def change
    create_table :umkm_market_images do |t|
    t.string :image
    t.boolean :is_thumbnail, deafult: false
    t.references :umkm_market, index: true
    t.datetime :deleted_at
    t.timestamps
    end
  end
end
