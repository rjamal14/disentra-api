class RemoveReferenceInUserProfile < ActiveRecord::Migration[6.0]
  def change
    remove_column :user_profiles, :job_position_id_id
    remove_column :user_profiles, :branch_office_id_id
    add_reference :user_profiles, :job_position, index: true
    add_reference :user_profiles, :branch_office, index: true
  end
end
