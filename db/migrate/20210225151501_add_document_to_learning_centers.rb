class AddDocumentToLearningCenters < ActiveRecord::Migration[6.0]
  def change
    add_column :learning_centers, :document, :string
  end
end
