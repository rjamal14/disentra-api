MONTHS = [
  { en: 'January', id: 'Januari', number: 1 },
  { en: 'February', id: 'Februari', number: 2 },
  { en: 'March', id: 'Maret', number: 3 },
  { en: 'April', id: 'April', number: 4 },
  { en: 'May', id: 'Mei', number: 5 },
  { en: 'June', id: 'Juni', number: 6 },
  { en: 'July', id: 'Juli', number: 7 },
  { en: 'August', id: 'Agustus', number: 8 },
  { en: 'September', id: 'September', number: 9 },
  { en: 'October', id: 'Oktober', number: 10 },
  { en: 'November', id: 'November', number: 11 },
  { en: 'December', id: 'Desember', number: 12 }
].freeze
