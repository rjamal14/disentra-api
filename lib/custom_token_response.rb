module CustomTokenResponse
  def body
    user = User.find(@token.resource_owner_id)

    additional_data = {
      data: UserSerializer.new(user)
    }

    super.merge(additional_data)
  end
end
