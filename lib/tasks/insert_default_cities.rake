namespace :default_cities do
  require 'csv'
  desc 'inser initial cities'
  task initial_input: :environment do
    file = Rails.root.join('public', 'initial_data', 'provinces.csv')
    CSV.parse(File.read(file), headers: false).each_with_index do |row, _idx|
      City.find_or_create_by(province_id: row[1], name: row[2])
    end
  end
end
