namespace :default_provinces do
  require 'csv'
  desc 'inser initial province'
  task initial_input: :environment do
    file = Rails.root.join('public', 'initial_data', 'provinces.csv')
    puts file
    CSV.parse(File.read(file), headers: false).each_with_index do |prov, _idx|
      Province.create(name: prov[1].upcase)
    end
  end
end
