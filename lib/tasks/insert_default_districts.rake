namespace :default_districts do
  require 'csv'
  desc 'insert initial districts'
  task initial_input: :environment do
    empty_cities = []
    file = Rails.root.join('public', 'initial_data', 'districts.csv')
    CSV.parse(File.read(file), headers: false).each_with_index do |row, _idx|
      city_id = City.find_by(province_id: row[1], name: row[2]).try(:id)
      empty_cities << row[3] if city_id.blank?
      next if city_id.blank?

      District.find_or_create_by(city_id: city_id, name: row[3])
    end

    puts 'Berikut ini adalah kecamatan yang tidak berhasil dibuat'
    puts empty_cities
  end
end
