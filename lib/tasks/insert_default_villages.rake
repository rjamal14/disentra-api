namespace :default_villages do
  require 'csv'
  desc 'inser initial villages'
  task initial_input: :environment do
    empty_cities    = []
    empty_districts = []

    file = Rails.root.join('public', 'initial_data', 'villages.csv')
    CSV.parse(File.read(file), headers: false).each_with_index do |row, _idx|
      city_id = City.find_by(province_id: row[1], name: row[2]).try(:id)
      empty_cities << row[2] if city_id.blank?
      next if city_id.blank?

      district_id = District.find_by(city_id: city_id, name: row[3]).try(:id)
      empty_districts << row[3] if district_id.blank?
      next if district_id.blank?

      Village.find_or_create_by(
        district_id: district_id,
        name: row[4],
        postal_code: row[5]
      )
    end

    puts 'Berikut ini adalah kota yang tidak berhasil dibuat'
    puts empty_cities

    puts 'Berikut ini adalah kecamatan yang tidak berhasil dibuat'
    puts empty_districts
  end
end
