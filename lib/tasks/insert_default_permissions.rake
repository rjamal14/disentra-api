# frozen_string_literal: true

namespace :default_permission do
  desc 'Manage permission'
  actions = %w[read create update destroy]

  object_full_access   = [{ name: 'Role', description: 'Manajemen User - Role' },
                          { name: 'BussinessTalk', description: 'Manajemen Konten - Bincang Bisnis' },
                          { name: 'LearningCenter', description: 'Manajemen Konten - Bincang Bisnis' },
                          { name: 'News', description: 'Manajemen Konten - Berita' },
                          { name: 'UmkmMarket', description: 'Manajemen Konten - Pasar UMKM' },
                          { name: 'Province', description: 'Master Data - Provinsi' },
                          { name: 'City', description: 'Master Data - Kota/Kabupaten' },
                          { name: 'District', description: 'Master Data - Kecamatan' },
                          { name: 'Village', description: 'Master Data - Kelurahan' },
                          { name: 'BusinessField', description: 'Master Data - Bidang Usaha' },
                          { name: 'JobPosition', description: 'Master Data - Jabatan' }]
  object_update_create = [{ name: 'UserAdmin', description: 'Manajemen User - Admin' }]
  object_only_update   = [{ name: 'UserUmkm', description: 'Manajemen User - UMKM' }]
  object_second_type   = object_only_update + object_update_create + object_full_access
  object_third_type    = object_update_create + object_full_access
  task create_features: :environment do
    actions.each do |action|
      case action
      when 'read'
        object_second_type.each do |object|
          Permission.find_or_create_by(permission_type: object[:name], action_name: action,
                                       description: object[:description])
        end
      when 'create'
        object_third_type.each do |object|
          Permission.find_or_create_by(permission_type: object[:name], action_name: action,
                                       description: object[:description])
        end
      when 'update'
        object_second_type.each do |object|
          Permission.find_or_create_by(permission_type: object[:name], action_name: action,
                                       description: object[:description])
        end
      when 'destroy'
        object_full_access.each do |object|
          Permission.find_or_create_by(permission_type: object[:name], action_name: action,
                                       description: object[:description])
        end
      end
    end
  end
end
