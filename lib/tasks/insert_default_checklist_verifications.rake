# frozen_string_literal: true

namespace :default_checklist_verifications do
  desc 'Manage checklist verifications'

  checklists = ['Foto KTP', 'Nomor Rekening Bank BJB', 'Nama Usaha',
                'Deskripsi Usaha', 'Nomor Handphone Usaha', 'Alamat']
  task create_features: :environment do
    checklists.each do |checklist|
      ChecklistVerification.find_or_create_by(name: checklist)
    end
  end
end
