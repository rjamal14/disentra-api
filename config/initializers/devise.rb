# frozen_string_literal: true

Devise.setup do |config|
  config.mailer_sender = ENV['MAILER_SENDER']

  config.mailer = 'UserMailer'

  require 'devise/orm/active_record'

  config.case_insensitive_keys = [:email]
  config.strip_whitespace_keys = [:email]
  config.skip_session_storage = [:http_auth]
  config.stretches = Rails.env.test? ? 1 : 12
  config.reconfirmable = true
  config.expire_all_remember_me_on_sign_out = true
  config.password_length = 5..128
  config.email_regexp = /\A[^@\s]+@[^@\s]+\z/
  config.lock_strategy = :failed_attempts
  config.unlock_strategy = :email
  config.maximum_attempts = 5
  config.reset_password_keys = [:email]
  config.reset_password_within = 6.hours
  config.sign_in_after_reset_password = false
  config.scoped_views = true
  config.sign_out_via = :delete
  config.omniauth_path_prefix = '/api/v1/users/auth'

  config.omniauth :google_oauth2, ENV['GOOGLE_CLIENT_ID'], ENV['GOOGLE_CLIENT_SECRET'],
                  { provider_ignores_state: true,
                    scope: 'email' }
end
