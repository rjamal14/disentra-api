# frozen_string_literal: true

CarrierWave.configure do |config|
  config.fog_provider = 'fog/aws'
  config.fog_credentials = {
    provider: 'AWS',
    aws_access_key_id: ENV['ACCESS_KEY'],
    aws_secret_access_key: ENV['SECRET_KEY'],
    region: ENV['DO_REGION'],
    endpoint: ENV['END_POINT']
  }
  config.fog_directory = ENV['DO_BUCKET']
  config.fog_public = true
  config.asset_host = ENV['ASSET_HOST']
  config.fog_attributes = { 'Cache-Control' => 'max-age=315576000' }
end
