# frozen_string_literal: true

Doorkeeper.configure do
  # Change the ORM that doorkeeper will use (needs plugins)
  orm :active_record

  # if no scope was requested, this will be the default
  default_scopes :admin

  # other available scopes
  optional_scopes :mobile

  resource_owner_from_credentials do |_routes|
    valid_user  = nil
    result      = Doorkeeper::LoginService.new(params: params).perform
    err_type    = result[:err_type] if result.present?
    valid_user  = result[:user] if result.present?

    raise Doorkeeper::Errors::DoorkeeperError, err_type if err_type.present?

    valid_user
  end

  grant_flows %w[password client_credentials]

  skip_authorization do
    true
  end
end

Doorkeeper::OAuth::TokenResponse.prepend CustomTokenResponse
