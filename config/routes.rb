# frozen_string_literal: true

Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  use_doorkeeper do
    # it accepts :authorizations, :tokens, :token_info, :applications and :authorized_applications
    controllers tokens: 'tokens'
  end

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      devise_for :users, controllers: {
        registrations: 'api/v1/users/registrations',
        confirmations: 'api/v1/users/confirmations',
        passwords: 'api/v1/users/passwords',
        omniauth_callbacks: 'api/v1/users/omniauth_callbacks'
      }

      get  'user', to: 'user#show'
      put  'user', to: 'user#update'
      patch 'user', to: 'user#update_password'

      resources :banners
      resources :business_fields do
        collection do
          get :all_business_field
        end
      end

      resources :business_talks
      resources :provinces
      resources :checklist_verifications, only: %i[index]
      resources :cities
      resources :districts
      resources :job_positions do
        collection do
          get :all_job_position
        end
      end

      resources :learning_centers
      resources :news do
        collection do
          post :collect
        end
      end
      resources :permissions, only: %i[index]
      resources :roles do
        collection do
          get :all_role
        end
      end

      resources :umkm_markets
      resources :user_admins
      resources :user_umkms do
        collection do
          get :business_verification_counter
        end

        member do
          get :business_verification_list
        end
      end
      resources :villages

      namespace :mobile do
        get  'notifications_count', to: 'notifications#notifications_count'
        post 'read_all_notifications', to: 'notifications#read_all_notifications'
        post 'join_business_talk', to: 'business_talks#create_participant'
        resources :banners, only: %i[index]
        resources :business_clinics, only: %i[index]
        resources :business_fields, only: %i[index]
        resources :business_talks, only: %i[index]
        resources :cities, only: %i[index] do
          collection do
            get :search_index
          end
        end

        resources :districts, only: %i[index]

        resources :learning_centers, only: %i[index] do
          collection do
            post :content_view
          end
        end

        resources :provinces, only: %i[index]
        resources :umkm_markets, only: %i[index show]
        resources :villages, only: %i[index]
        resources :notifications, only: %i[index show]
        resources :news, only: %i[index show]
      end
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
