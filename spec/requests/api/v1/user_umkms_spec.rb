require 'swagger_helper'

RSpec.describe 'api/v1/user_umkms', type: :request do
  path '/api/v1/user_umkms?search={search}' do
    get('Search user_umkms') do
      tags 'User - Type Umkm'
      security [Bearer: {}]
      parameter name: 'search', in: :query, type: :string
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/user_umkms?' do
    get('Filter user_umkms') do
      tags 'User - Type Umkm'
      security [Bearer: {}]
      parameter name: 'q[name_cont]', in: :query, type: :string
      parameter name: 'q[user_profile_nip_cont]', in: :query, type: :string
      parameter name: 'q[email_cont]', in: :query, type: :string
      parameter name: 'q[phone_number_eq]', in: :query, type: :string
      parameter name: 'q[verification_status_eq]', in: :query, type: :string
      parameter name: 'q[user_profile_job_position_id_eq]', in: :query, type: :string
      parameter name: 'role_name', in: :query, type: :string
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/user_umkms' do
    get('list user_umkms') do
      tags 'User - Type Umkm'
      security [Bearer: {}]
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/user_umkms/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('show user_umkm') do
      tags 'User - Type Umkm'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    patch('business verification') do
      tags 'User - Type Umkm'
      security [Bearer: {}]
      parameter name: 'user[verification_status]', in: :formData, type: :string, enum: %i[rejected], required: true
      parameter name: 'user[business_verifications_attributes][0][checklist_verification_id]', in: :formData, type: :integer
      parameter name: 'user[business_verifications_attributes][0][note]', in: :formData, type: :string
      parameter name: 'user[business_verifications_attributes][1][checklist_verification_id]', in: :formData, type: :integer
      parameter name: 'user[business_verifications_attributes][1][note]', in: :formData, type: :string
      parameter name: 'user[business_verifications_attributes][2][checklist_verification_id]', in: :formData, type: :integer
      parameter name: 'user[business_verifications_attributes][2][note]', in: :formData, type: :string
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('active or suspend and approve business verification user_umkm') do
      tags 'User - Type Umkm'
      security [Bearer: {}]
      parameter name: 'user[status]', in: :formData, type: :string, enum: %i[active suspend]
      parameter name: 'user[verification_status]', in: :formData, type: :string, enum: %i[approved]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/user_umkms/business_verification_counter' do
    get('show business verification counter') do
      tags 'User - Type Umkm'
      security [Bearer: {}]
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/user_umkms/{id}/business_verification_list' do
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('show business verifications') do
      tags 'User - Type Umkm'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
