# frozen_string_literal: true

require 'swagger_helper'

describe 'Registration' do
  path '/api/v1/users/' do
    post 'User Registration' do
      tags 'User - Registration'
      consumes 'multipart/form-data'
      parameter name: 'user[email]', in: :formData,
                type: :string, required: true
      parameter name: 'user[password]', in: :formData,
                type: :string, required: true
      parameter name: 'user[password_confirmation]', in: :formData,
                type: :string
      parameter name: 'user[name]', in: :formData, type: :string
      parameter name: 'user[phone_number]', in: :formData, type: :string
      parameter name: 'user[user_type]', in: :formData,
                type: :string, default: 'mobile',
                description: 'admin / mobile'

      response 200, 'Registration Success' do
        examples 'application/json' => {
          data: {
            id: 23,
            email: 'test-user1@mailinator.com',
            user_type: 'mobile',
            name: 'Delon Tanuwedja',
            phone_number: '08122111',
            created_at: '2021-02-06T12:50:58.543+07:00',
            updated_at: '2021-02-06T12:50:58.543+07:00'
          }
        }
        run_test!
      end
    end
  end

  path '/api/v1/users/confirmation?confirmation_token={token}' do
    get 'User Confirm Registration' do
      tags 'User - Registration'
      consumes 'application/json'
      parameter name: :token, in: :path, type: :string
      response 200, 'Successfull Get Reset Password Token' do
        examples 'application/json' => {
          user: {
            id: 23,
            email: 'test-user1@mailinator.com',
            user_type: 'mobile',
            name: 'Delon Tanuwedja',
            phone_number: '08122111',
            photo: {
              url: '/uploads/user/photo/23/photo.png'
            },
            user_profile: {
              id: 6,
              user_id: 23,
              account_number: '65333333',
              identity_photo: {
                url: '/uploads/user_profile/identity_photo/6/identity.png'
              },
              bussiness_name: 'Delon Cookies',
              bussiness_description: 'Cookies and pastry',
              bussiness_address: 'Jl. Kota Kembang No.43, Bandung',
              longitude: 107.619125,
              latitude: -6.917464,
              province_id: 1,
              province_name: 'Jawa Barat',
              city_id: 2,
              city_name: 'Bandung',
              district_id: 3,
              district_name: 'Batununggal',
              village_id: 4,
              village_name: 'Batununggal Barat',
              created_at: '2021-02-06T12:50:58.547+07:00',
              updated_at: '2021-02-06T12:50:58.547+07:00'
            },
            created_at: '2021-02-06T12:50:58.543+07:00',
            updated_at: '2021-02-06T12:50:58.543+07:00'
          }
        }
        run_test!
      end
    end
  end
end
