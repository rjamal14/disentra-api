# frozen_string_literal: true

require 'swagger_helper'

describe 'User Reset Password' do
  path '/api/v1/users/password' do
    post 'User Ask Reset Password' do
      tags 'User - Reset Password'
      consumes 'application/json'
      parameter name: :email, in: :body, schema: {
        type: :object,
        properties: {
          email: { type: :string, example: 'test-user@mailinator.com' },
          scope: { type: :string, example: 'mobile' }
        },
        required: %w[email]
      }
      response 200, 'Request Reset Password Success' do
        examples 'application/json' => {
          success: 'request_reset_password_success'
        }
        run_test!
      end
    end

    put 'User Change Password' do
      tags 'User - Reset Password'
      consumes 'application/json'
      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: {
          reset_password_token: { type: :string,
                                  example: '0c90886ea96e051467fc87f5e7ae25979c5fd399fac2b8155f972fe47e0d7720' },
          scope: { type: :string, example: 'mobile' },
          user: {
            type: :object,
            properties: {
              password: { type: :string, example: 'pass123' },
              password_confirmation: { type: :string, example: 'pass123' }
            }
          }
        },
        required: %w[reset_password_token password password_confirmation]
      }
      response 200, 'Successful Reset Password' do
        examples 'application/json' => {
          success: 'reset_password_success'
        }
        run_test!
      end
    end
  end

  path '/api/v1/users/password/edit?reset_password_token={token}&scope={scope}' do
    get 'User Show Reset Password Token' do
      tags 'User - Reset Password'
      consumes 'application/json'
      parameter name: :token, in: :path, type: :string
      parameter name: :scope, in: :path, type: :string, enum: %i[admin mobile], required: true
      response 200, 'Successfull Get Reset Password Token' do
        examples 'application/json' => {
          token: '0c90886ea96e051467fc87f5e7ae25979c5fd399fac2b8155f972fe47e0d7720',
          user: {
            id: 23,
            email: 'test-user1@mailinator.com',
            user_type: 'mobile',
            name: 'Delon Tanuwedja',
            phone_number: '08122111',
            photo: {
              url: '/uploads/user/photo/23/photo.png'
            },
            user_profile: {
              id: 6,
              user_id: 23,
              account_number: '65333333',
              identity_photo: {
                url: '/uploads/user_profile/identity_photo/6/identity.png'
              },
              bussiness_name: 'Delon Cookies',
              bussiness_description: 'Cookies and pastry',
              bussiness_address: 'Jl. Kota Kembang No.43, Bandung',
              longitude: 107.619125,
              latitude: -6.917464,
              created_at: '2021-02-06T12:50:58.547+07:00',
              updated_at: '2021-02-06T12:50:58.547+07:00'
            },
            created_at: '2021-02-06T12:50:58.543+07:00',
            updated_at: '2021-02-06T12:50:58.543+07:00'
          }
        }
        run_test!
      end
    end
  end
end
