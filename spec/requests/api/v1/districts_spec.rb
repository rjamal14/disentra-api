require 'swagger_helper'

RSpec.describe 'api/v1/districts', type: :request do
  path '/api/v1/districts?search={search}' do
    get('Search districts') do
      tags 'Master - Districts'
      security [Bearer: {}]
      parameter name: 'search', in: :query, type: :string
      parameter name: 'page', in: :query, type: :integer, example: 1
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/districts?' do
    get('Filter districts') do
      tags 'Master - Districts'
      security [Bearer: {}]
      parameter name: 'q[name_cont]', in: :query, type: :string
      parameter name: 'q[city_id_eq]', in: :query, type: :integer
      parameter name: 'page', in: :query, type: :integer, example: 1
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/districts' do
    get('list districts') do
      tags 'Master - Districts'
      security [Bearer: {}]
      parameter name: 'page', in: :query, type: :integer, example: 1
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('create district') do
      tags 'Master - Districts'
      security [Bearer: {}]
      consumes 'application/json'
      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string, example: 'Batununggal' },
          city_id: { type: :string, example: '1' }
        },
        required: %w[name city_id]
      }
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/districts/{id}' do
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('show district') do
      tags 'Master - Districts'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    patch('update district') do
      tags 'Master - Districts'
      security [Bearer: {}]
      consumes 'application/json'
      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string, example: 'Batununggal' },
          city_id: { type: :string, example: '1' }
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('delete district') do
      tags 'Master - Districts'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
