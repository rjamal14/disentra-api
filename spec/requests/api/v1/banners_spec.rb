require 'swagger_helper'

RSpec.describe 'api/v1/banners', type: :request do
  path '/api/v1/banners?' do
    get('Filter banners') do
      tags 'Master - Banners'
      security [Bearer: {}]
      parameter name: 'banner_type', in: :query, type: :string, enum: %i[home clinic talk learning umkm]
      parameter name: 'q[status_eq]', in: :query, type: :string, enum: %i[active inactive]
      parameter name: 'page', in: :query, type: :integer, example: 1
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/banners' do
    get('list banners by type') do
      tags 'Master - Banners'
      security [Bearer: {}]
      parameter name: 'banner_type', in: :query, type: :string, enum: %i[home clinic talk learning umkm]
      parameter name: 'page', in: :query, type: :integer, example: 1
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('create banner') do
      tags 'Master - Banners'
      security [Bearer: {}]
      parameter name: 'banner[banner_type]', in: :formData, type: :string, enum: %i[home clinic talk learning umkm],
                required: true
      parameter name: 'banner[link]', in: :formData, type: :string
      parameter name: 'banner[image]', in: :formData, type: :file, required: true
      parameter name: 'banner[status]', in: :formData, type: :string, enum: %i[active inactive], required: true

      response(201, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/banners/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('show banner') do
      tags 'Master - Banners'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    patch('update banner') do
      tags 'Master - Banners'
      security [Bearer: {}]
      parameter name: 'banner[banner_type]', in: :formData, type: :string, enum: %i[home clinic talk learning umkm]
      parameter name: 'banner[link]', in: :formData, type: :string
      parameter name: 'banner[image]', in: :formData, type: :file
      parameter name: 'banner[status]', in: :formData, type: :string, enum: %i[active inactive]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('delete banner') do
      tags 'Master - Banners'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
