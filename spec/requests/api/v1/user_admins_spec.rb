require 'swagger_helper'

RSpec.describe 'api/v1/user_admins', type: :request do
  path '/api/v1/user_admins?search={search}' do
    get('Search user_admins') do
      tags 'User - Type Admin'
      security [Bearer: {}]
      parameter name: 'search', in: :query, type: :string
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/user_admins?' do
    get('Filter user_admins') do
      tags 'User - Type Admin'
      security [Bearer: {}]
      parameter name: 'q[name_or_user_profle_nip_cont]', in: :query, type: :string
      parameter name: 'q[email_eq]', in: :query, type: :string
      parameter name: 'q[phone_number_eq]', in: :query, type: :string
      parameter name: 'q[user_profile_job_position_id_eq]', in: :query, type: :string
      parameter name: 'q[job_position_id_eq]', in: :query, type: :string
      parameter name: 'role_name', in: :query, type: :string
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/user_admins' do
    get('list user_admins') do
      tags 'User - Type Admin'
      security [Bearer: {}]
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('create user_admin') do
      tags 'User - Type Admin'
      security [Bearer: {}]
      parameter name: 'user[name]', in: :formData, type: :string, required: true
      parameter name: 'user[email]', in: :formData, type: :string, required: true
      parameter name: 'user[phone_number]', in: :formData, type: :string, required: true
      parameter name: 'user[photo]', in: :formData, type: :file, required: true
      parameter name: 'user[status]', in: :formData, type: :string, enum: %i[active suspend]
      parameter name: 'user[user_profile_attributes][nip]', in: :formData, type: :integer
      parameter name: 'user[user_profile_attributes][job_position_id]', in: :formData, type: :integer
      parameter name: 'user[user_profile_attributes][account_type]', in: :formData, type: :string,
                enum: %i[bjb non_bjb]
      parameter name: 'user[user_profile_attributes][consultant_profession]', in: :formData, type: :string
      parameter name: 'user[user_profile_attributes][account_description]', in: :formData, type: :string
      parameter name: 'user[role_ids]', in: :formData, type: :array, items: { type: :integer }, required: true
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/user_admins/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('show user_admin') do
      tags 'User - Type Admin'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    patch('update user_admin') do
      tags 'User - Type Admin'
      security [Bearer: {}]
      parameter name: 'user[name]', in: :formData, type: :string
      parameter name: 'user[email]', in: :formData, type: :string
      parameter name: 'user[phone_number]', in: :formData, type: :string
      parameter name: 'user[photo]', in: :formData, type: :file
      parameter name: 'user[status]', in: :formData, type: :string, enum: %i[active suspend]
      parameter name: 'user[user_profile_attributes][id]', in: :formData, type: :integer, required: true
      parameter name: 'user[user_profile_attributes][nip]', in: :formData, type: :integer
      parameter name: 'user[user_profile_attributes][job_position_id]', in: :formData, type: :integer
      parameter name: 'user[user_profile_attributes][account_type]', in: :formData, type: :string,
                enum: %i[bjb non_bjb]
      parameter name: 'user[user_profile_attributes][consultant_profession]', in: :formData, type: :string
      parameter name: 'user[user_profile_attributes][account_description]', in: :formData, type: :string
      parameter name: 'user[role_ids][]', in: :formData, type: :array, items: { type: :integer }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('active or suspend user_admin') do
      tags 'User - Type Admin'
      security [Bearer: {}]
      parameter name: 'user[status]', in: :formData, type: :string, enum: %i[active suspend]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('delete user_admin') do
      tags 'User - Type Admin'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
