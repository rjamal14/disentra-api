require 'swagger_helper'

RSpec.describe 'api/v1/learning_centers', type: :request do
  path '/api/v1/learning_centers?search={search}' do
    get('Search learning_centers') do
      tags 'Master - Learning Centers'
      security [Bearer: {}]
      parameter name: 'search', in: :query, type: :string
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/learning_centers?' do
    get('Filter learning_centers') do
      tags 'Master - Learning Centers'
      security [Bearer: {}]
      parameter name: 'q[content_type_eq]', in: :query, type: :string, enum: %i[video ebook news]
      parameter name: 'q[status_eq]', in: :query, type: :string, enum: %i[show hide]
      parameter name: 'q[title_cont]', in: :query, type: :string
      parameter name: 'q[author_name_cont]', in: :query, type: :string
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/learning_centers' do
    get('list learning_centers') do
      tags 'Master - Learning Centers'
      security [Bearer: {}]
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('create learning_center') do
      tags 'Master - Learning Centers'
      security [Bearer: {}]
      parameter name: 'learning_center[content_type]', in: :formData, type: :string, enum: %i[video ebook news]
      parameter name: 'learning_center[status]', in: :formData, type: :string, enum: %i[show hide]
      parameter name: 'learning_center[link]', in: :formData, type: :string
      parameter name: 'learning_center[document]', in: :formData, type: :file
      parameter name: 'learning_center[title]', in: :formData, type: :string
      parameter name: 'learning_center[image]', in: :formData, type: :file
      parameter name: 'learning_center[author_name]', in: :formData, type: :string
      parameter name: 'learning_center[description]', in: :formData, type: :string
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/learning_centers/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('show learning_center') do
      tags 'Master - Learning Centers'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    patch('update learning_center') do
      tags 'Master - Learning Centers'
      security [Bearer: {}]
      parameter name: 'learning_center[content_type]', in: :formData, type: :string, enum: %i[video ebook news]
      parameter name: 'learning_center[status]', in: :formData, type: :string, enum: %i[show hide]
      parameter name: 'learning_center[link]', in: :formData, type: :string
      parameter name: 'learning_center[document]', in: :formData, type: :file
      parameter name: 'learning_center[title]', in: :formData, type: :string
      parameter name: 'learning_center[image]', in: :formData, type: :file
      parameter name: 'learning_center[author_name]', in: :formData, type: :string
      parameter name: 'learning_center[description]', in: :formData, type: :string
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('delete learning_center') do
      tags 'Master - Learning Centers'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
