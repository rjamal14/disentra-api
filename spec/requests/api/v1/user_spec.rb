require 'swagger_helper'

RSpec.describe 'api/v1/user', type: :request do
  path '/api/v1/user' do
    get 'show user' do
      tags 'User - Detail & User Profile'
      security [Bearer: {}]
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('update user') do
      tags 'User - Detail & User Profile'
      security [Bearer: {}]
      parameter name: 'user[name]', in: :formData, type: :string
      parameter name: 'user[phone_number]', in: :formData, type: :string
      parameter name: 'user[photo]', in: :formData, type: :file
      parameter name: 'user[user_profile_attributes][id]', in: :formData, type: :integer
      parameter name: 'user[user_profile_attributes][account_number]', in: :formData, type: :integer
      parameter name: 'user[user_profile_attributes][whatsapp_number]', in: :formData, type: :integer
      parameter name: 'user[user_profile_attributes][identity_photo]', in: :formData, type: :file
      parameter name: 'user[user_profile_attributes][bussiness_name]', in: :formData, type: :string
      parameter name: 'user[user_profile_attributes][bussiness_description]', in: :formData, type: :text
      parameter name: 'user[user_profile_attributes][bussiness_address]', in: :formData, type: :text
      parameter name: 'user[user_profile_attributes][longitude]', in: :formData, type: :float
      parameter name: 'user[user_profile_attributes][latitude]', in: :formData, type: :float
      parameter name: 'user[user_profile_attributes][province_id]', in: :formData, type: :number
      parameter name: 'user[user_profile_attributes][city_id]', in: :formData, type: :number
      parameter name: 'user[user_profile_attributes][district_id]', in: :formData, type: :number
      parameter name: 'user[user_profile_attributes][village_id]', in: :formData, type: :number
      parameter name: 'user[business_photos_attributes][0][id]', in: :formData, type: :integer
      parameter name: 'user[business_photos_attributes][0][image]', in: :formData, type: :file
      parameter name: 'user[business_photos_attributes][0][_destroy]', in: :formData, type: :boolean
      parameter name: 'user[business_photos_attributes][1][id]', in: :formData, type: :integer
      parameter name: 'user[business_photos_attributes][1][image]', in: :formData, type: :file
      parameter name: 'user[business_photos_attributes][1][_destroy]', in: :formData, type: :boolean
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    patch('update password user') do
      tags 'User - Detail & User Profile'
      security [Bearer: {}]
      parameter name: 'user[current_password]', in: :formData, type: :string
      parameter name: 'user[password]', in: :formData, type: :string
      parameter name: 'user[password_confirmation]', in: :formData, type: :string
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
