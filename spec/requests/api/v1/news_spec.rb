require 'swagger_helper'

RSpec.describe 'api/v1/news', type: :request do
  path '/api/v1/news/collect' do
    post('collect news') do
      tags 'Master - News'
      security [Bearer: {}]
      parameter name: 'page', in: :query, type: :integer
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/news?search={search}' do
    get('Search news') do
      tags 'Master - News'
      security [Bearer: {}]
      parameter name: 'search', in: :query, type: :string
      parameter name: 'page', in: :query, type: :integer, example: 1
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/news' do
    get('list news') do
      tags 'Master - News'
      security [Bearer: {}]
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('create news') do
      tags 'Master - News'
      security [Bearer: {}]
      parameter name: 'news[title]', in: :formData, type: :string
      parameter name: 'news[body]', in: :formData, type: :text
      parameter name: 'news[image]', in: :formData, type: :file
      parameter name: 'news[published_at]', in: :formData, type: :date
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/news/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'
    get('show news') do
      tags 'Master - News'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    patch('update news') do
      tags 'Master - News'
      security [Bearer: {}]
      parameter name: 'news[title]', in: :formData, type: :string
      parameter name: 'news[body]', in: :formData, type: :text
      parameter name: 'news[image]', in: :formData, type: :file
      parameter name: 'news[published_at]', in: :formData, type: :date
      response(200, 'successful') do
        let(:id) { '123' }
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('delete news') do
      tags 'Master - News'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
