require 'swagger_helper'

RSpec.describe 'api/v1/roles', type: :request do
  path '/api/v1/roles?search={search}' do
    get('Search roles') do
      tags 'Management - Roles'
      security [Bearer: {}]
      parameter name: 'search', in: :query, type: :string
      parameter name: 'page', in: :query, type: :integer, example: 1
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/roles?' do
    get('Filter roles') do
      tags 'Management - Roles'
      security [Bearer: {}]
      parameter name: 'q[name_cont]', in: :query, type: :string
      parameter name: 'page', in: :query, type: :integer, example: 1
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/roles/all_role?' do
    get('All Roles') do
      tags 'Management - Roles'
      security [Bearer: {}]
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/roles' do
    get('list roles') do
      tags 'Management - Roles'
      security [Bearer: {}]
      parameter name: 'page', in: :query, type: :integer, example: 1
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('create role') do
      tags 'Management - Roles'
      security [Bearer: {}]
      consumes 'application/json'
      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string, example: 'Admin' },
          description: { type: :string, example: 'Deskripsi Role' },
          role_permissions_attributes: {
            type: :array,
            items: {
              properties: {
                permission_id: { type: :integer, example: 1 }
              }
            }
          }
        },
        required: %w[name]
      }
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/roles/{id}' do
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('show role') do
      tags 'Management - Roles'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    patch('update role') do
      tags 'Management - Roles'
      security [Bearer: {}]
      consumes 'application/json'
      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string, example: 'Admin' },
          description: { type: :string, example: 'Deskripsi Role' },
          role_permissions_attributes: {
            type: :array,
            items: {
              properties: {
                permission_id: { type: :integer, example: 1 }
              }
            }
          }
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('delete role') do
      tags 'Management - Roles'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
