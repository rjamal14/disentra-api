require 'swagger_helper'

RSpec.describe 'api/v1/checklist_verifications', type: :request do
  path '/api/v1/checklist_verifications' do
    get('list checklist_verifications') do
      tags 'Master - Checklist Verifications'
      security [Bearer: {}]
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
