require 'swagger_helper'

RSpec.describe 'api/v1/business_talks', type: :request do
  path '/api/v1/business_talks?search={search}' do
    get('Search business_talks') do
      tags 'Master - Business Talks'
      security [Bearer: {}]
      parameter name: 'search', in: :query, type: :string
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/business_talks?' do
    get('Filter business_talks') do
      tags 'Master - Business Talks'
      security [Bearer: {}]
      parameter name: 'q[talk_type_eq]', in: :query, type: :string, enum: %i[grupchat sentraminar]
      parameter name: 'q[link_eq]', in: :query, type: :string
      parameter name: 'q[title_cont]', in: :query, type: :string
      parameter name: 'q[theme_eq]', in: :query, type: :string
      parameter name: 'q[event_on_lteq]', in: :query, type: :string
      parameter name: 'q[event_on_gteq]', in: :query, type: :string
      parameter name: 'q[start_time_eq]', in: :query, type: :time
      parameter name: 'q[end_time_eq]', in: :query, type: :time
      parameter name: 'q[health_protocol_eq]', in: :query, type: :boolean
      parameter name: 'q[max_participant_eq]', in: :query, type: :integer
      parameter name: 'q[speaker_cont]', in: :query, type: :string
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/business_talks' do
    get('list business_talks') do
      tags 'Master - Business Talks'
      security [Bearer: {}]
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('create business_talk') do
      tags 'Master - Business Talks'
      security [Bearer: {}]
      parameter name: 'business_talk[talk_type]', in: :formData, type: :string, enum: %i[grupchat sentraminar]
      parameter name: 'business_talk[link]', in: :formData, type: :string, required: true
      parameter name: 'business_talk[title]', in: :formData, type: :string, required: true
      parameter name: 'business_talk[theme]', in: :formData, type: :string, required: true
      parameter name: 'business_talk[event_on]', in: :formData, type: :string, required: true
      parameter name: 'business_talk[start_time]', in: :formData, type: :string, required: true
      parameter name: 'business_talk[end_time]', in: :formData, type: :string, required: true
      parameter name: 'business_talk[image]', in: :formData, type: :file, required: true
      parameter name: 'business_talk[health_protocol]', in: :formData, type: :boolean
      parameter name: 'business_talk[max_participant]', in: :formData, type: :integer, required: true
      parameter name: 'business_talk[speaker]', in: :formData, type: :string, required: true
      parameter name: 'business_talk[description]', in: :formData, type: :string, required: true
      parameter name: 'business_talk[status]', in: :formData, type: :string, enum: %i[show hide], default: :show
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/business_talks/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('show business_talk') do
      tags 'Master - Business Talks'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    patch('update business_talk') do
      tags 'Master - Business Talks'
      security [Bearer: {}]
      parameter name: 'business_talk[talk_type]', in: :formData, type: :string, enum: %i[grupchat sentraminar]
      parameter name: 'business_talk[link]', in: :formData, type: :string
      parameter name: 'business_talk[title]', in: :formData, type: :string
      parameter name: 'business_talk[theme]', in: :formData, type: :string
      parameter name: 'business_talk[event_on]', in: :formData, type: :string
      parameter name: 'business_talk[start_time]', in: :formData, type: :string
      parameter name: 'business_talk[end_time]', in: :formData, type: :string
      parameter name: 'business_talk[image]', in: :formData, type: :file
      parameter name: 'business_talk[health_protocol]', in: :formData, type: :boolean
      parameter name: 'business_talk[max_participant]', in: :formData, type: :integer
      parameter name: 'business_talk[speaker]', in: :formData, type: :string
      parameter name: 'business_talk[description]', in: :formData, type: :string
      parameter name: 'business_talk[status]', in: :formData, type: :string, enum: %i[show hide], default: :show
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('delete business_talk') do
      tags 'Master - Business Talks'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
