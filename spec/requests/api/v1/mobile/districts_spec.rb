require 'swagger_helper'

RSpec.describe 'api/v1/mobile/districts', type: :request do
  path '/api/v1/mobile/districts?' do
    get('Filter districts') do
      tags 'Mobile - Districts'
      security [Bearer: {}]
      parameter name: 'q[name_cont]', in: :query, type: :string
      parameter name: 'q[city_id_eq]', in: :query, type: :integer
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/mobile/districts' do
    get('list districts') do
      tags 'Mobile - Districts'
      security [Bearer: {}]
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
