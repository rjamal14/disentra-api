require 'swagger_helper'

RSpec.describe 'api/v1/mobile/banners', type: :request do
  path '/api/v1/mobile/banners?' do
    get('Filter banners') do
      tags 'Mobile - Banners'
      security [Bearer: {}]
      parameter name: 'banner_type', in: :query, type: :string, enum: %i[home clinic talk learning umkm]
      parameter name: 'q[status_eq]', in: :query, type: :string, enum: %i[active inactive]
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/mobile/banners' do
    get('list banners') do
      tags 'Mobile - Banners'
      security [Bearer: {}]
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
