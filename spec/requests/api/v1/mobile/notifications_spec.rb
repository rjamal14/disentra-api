require 'swagger_helper'

RSpec.describe 'api/v1/mobile/notifications', type: :request do
  path '/api/v1/mobile/notifications?' do
    get 'list notifications' do
      tags 'Mobile - Notifications'
      security [Bearer: {}]
      parameter name: 'page', in: :query, type: :integer, example: 1
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      response 200, 'successful' do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/mobile/notifications/{id}' do
    parameter name: 'id', in: :path, type: :string, description: 'Notification id'
    get 'show notification' do
      tags 'Mobile - Notifications'
      security [Bearer: {}]
      response 200, 'successful' do
        let(:id) { '123' }
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/mobile/notifications_count' do
    get 'notifications_count notification' do
      tags 'Mobile - Notifications'
      security [Bearer: {}]
      response 200, 'successful' do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/mobile/read_all_notifications' do
    post 'read_all_notifications notification' do
      tags 'Mobile - Notifications'
      security [Bearer: {}]
      response 200, 'successful' do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
