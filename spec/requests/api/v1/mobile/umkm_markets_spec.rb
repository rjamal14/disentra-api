require 'swagger_helper'

RSpec.describe 'api/v1/mobile/umkm_markets', type: :request do
  path '/api/v1/mobile/umkm_markets' do
    get('list umkm_markets') do
      tags 'Mobile - Umkm Markets'
      security [Bearer: {}]
      parameter name: 'search', in: :query, type: :string
      parameter name: 'q[name_cont]', in: :query, type: :string
      parameter name: 'q[link_cont]', in: :query, type: :string
      parameter name: 'q[description_cont]', in: :query, type: :string
      parameter name: 'q[business_field_id_eq]', in: :query, type: :integer
      parameter name: 'q[city_id_eq]', in: :query, type: :integer
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/mobile/umkm_markets/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('show umkm_market') do
      tags 'Mobile - Umkm Markets'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
