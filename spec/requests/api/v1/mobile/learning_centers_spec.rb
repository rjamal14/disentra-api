require 'swagger_helper'

RSpec.describe 'api/v1/mobile/learning_centers', type: :request do
  path '/api/v1/mobile/learning_centers?search={search}' do
    get('Search learning_centers') do
      tags 'Mobile - Learning Centers'
      security [Bearer: {}]
      parameter name: 'search', in: :query, type: :string
      parameter name: 'page', in: :query, type: :integer, example: 1
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/mobile/learning_centers?' do
    get('Filter learning_centers') do
      tags 'Mobile - Learning Centers'
      security [Bearer: {}]
      parameter name: 'q[content_type_eq]', in: :query, type: :string, enum: %i[video ebook news]
      parameter name: 'q[title_cont]', in: :query, type: :string
      parameter name: 'q[created_at_lteq]', in: :query, type: :string
      parameter name: 'q[created_at_gteq]', in: :query, type: :string
      parameter name: 'filter_time', in: :query, type: :string, enum: %i[this_week this_month this_week]
      parameter name: 'page', in: :query, type: :integer, example: 1
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/mobile/learning_centers/content_view' do
    post('views content learning_center') do
      tags 'Mobile - Learning Centers'
      security [Bearer: {}]
      parameter name: 'content_view[learning_center_id]', in: :formData, type: :integer, required: true
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
