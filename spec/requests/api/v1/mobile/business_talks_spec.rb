require 'swagger_helper'

RSpec.describe 'api/v1/mobile/business_talks', type: :request do
  path '/api/v1/mobile/business_talks?search={search}' do
    get('Search business_talks') do
      tags 'Mobile - Business Talks'
      security [Bearer: {}]
      parameter name: 'search', in: :query, type: :string
      parameter name: 'page', in: :query, type: :integer, example: 1
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/mobile/business_talks?' do
    get('Filter business_talks') do
      tags 'Mobile - Business Talks'
      security [Bearer: {}]
      parameter name: 'q[name_cont]', in: :query, type: :string
      parameter name: 'q[talk_type_eq]', in: :query, type: :string, enum: %i[grupchat sentraminar]
      parameter name: 'q[event_on_lteq]', in: :query, type: :string
      parameter name: 'q[event_on_gteq]', in: :query, type: :string
      parameter name: 'filter_time', in: :query, type: :string, enum: %i[this_week this_month this_week]
      parameter name: 'page', in: :query, type: :integer, example: 1
      parameter name: 'per_page', in: :query, type: :integer, example: 15
      parameter name: 'order_by', in: :query, type: :string, example: 'created_at'
      parameter name: 'order', in: :query, type: :string, enum: %i[desc asc], example: 'asc'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/mobile/business_talks' do
    get('show business_talk') do
      tags 'Mobile - Business Talks'
      security [Bearer: {}]
      parameter name: 'id', in: :path, type: :string, description: 'id'
      response(200, 'successful') do
        let(:id) { '1' }
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/mobile/join_business_talk' do
    post 'create_participant business_talk' do
      tags 'Mobile - Business Talks'
      security [Bearer: {}]
      parameter name: 'id', in: :formData, type: :string, description: 'id'
      response(200, 'successful') do
        let(:id) { '1' }
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
