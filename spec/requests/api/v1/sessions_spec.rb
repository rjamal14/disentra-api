# frozen_string_literal: true

require 'swagger_helper'

describe 'User Login' do
  path '/oauth/token' do
    post 'User Login' do
      tags 'User - Login'
      consumes 'application/json'
      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: {
          email: { type: :string, example: 'dixs3a@mailinator.com' },
          password: { type: :string, example: 'pass123' },
          scope: { type: :string, example: 'admin' },
          grant_type: { type: :string, example: 'password' },
          client_id: { type: :string, example: 'nSpbU34_5Punot7-RGhInSjcTrn9g-Eq5OEP_pDc6_g' },
          client_secret: { type: :string, example: 've8zBVewYDDXzr4TyySzxihmqZKgYw8t5KEHJYIYzew' }
        },
        required: %w[email password scope grant_type client_id client_secret]
      }
      response 200, 'Login Success' do
        examples 'application/json' => {
          access_token: 'yDTEWtAj5pjAb6mdzPveHIGwPTQQoIDNSqyR84b2gts',
          token_type: 'Bearer', expires_in: 7200,
          scope: 'admin', created_at: 1_612_703_430
        }
        run_test!
      end
    end
  end
end
