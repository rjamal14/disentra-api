# frozen_string_literal: true

require 'rails_helper'

RSpec.configure do |config|
  config.swagger_root = Rails.root.join('swagger').to_s
  config.swagger_docs = {
    'v1/swagger.json' => {
      swagger: '2.0',
      info: {
        title: 'Disentra Admin and Mobile',
        version: 'v1'
      },
      paths: {},
      securityDefinitions: {
        Bearer: {
          description: 'bearer token here ....',
          type: :apiKey,
          name: 'Authorization',
          in: :header
        }
      },
      servers: [
        {
          url: ENV['FULLHOST']
        }
      ]
    }
  }
  config.swagger_format = :json
end
