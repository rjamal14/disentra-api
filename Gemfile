# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.3'

gem 'active_model_serializers', '~> 0.10.10'
gem 'active_record_fix_integer_limit', '~> 0.1.7'
gem 'bootsnap', '>= 1.4.2', require: false
gem 'cancancan', '~> 2.0'
gem 'carrierwave', '~> 1.0'
gem 'carrierwave-aws', '~> 1.4.0'
gem 'daemons', '~> 1.3.1'
gem 'delayed_job_active_record', '~> 4.1.4'
gem 'devise', github: 'heartcombo/devise', branch: 'ca-omniauth-2'
gem 'doorkeeper', '~> 5.2.3'
gem 'dotenv-rails', '~> 2.7.6'
gem 'enumerize', '~> 2.1', '>= 2.1.2'
gem 'fcm', '0.0.6'
gem 'fog-aws', '~> 3.8.0'
gem 'kaminari', '~> 1.2.1'
gem 'kimurai', '1.4.0'
gem 'mini_magick', '~> 4.11.0'
gem 'omniauth-facebook', '~> 4.0'
gem 'omniauth-google-oauth2', '~> 0.8.0'
gem 'omniauth-rails_csrf_protection'
gem 'paranoia', '~> 2.2'
gem 'pg', '~> 1.2.3'
gem 'pg_search', '2.3.5'
gem 'premailer-rails', '~> 1.11.1'
gem 'puma', '~> 4.1'
gem 'rack-cors', '~> 1.1.1'
gem 'rails', '~> 6.0.3', '>= 6.0.3.4'
gem 'rails-i18n', '~> 6.0.0'
gem 'ransack', github: 'activerecord-hackery/ransack'
gem 'rolify', '5.2.0'
gem 'rollbar'
gem 'rswag', '2.4.0'
gem 'sendinblue'
gem 'yt', '~> 0.32.0'

group :development, :test do
  gem 'byebug', '~> 11.1.3', platforms: %i[mri mingw x64_mingw]
  gem 'rspec-rails', '4.0.2'
  gem 'rswag-specs', '2.4.0'
end

group :development do
  gem 'listen', '~> 3.2'
  gem 'rubocop-rails', '~> 2.8.1', require: false
  gem 'spring', '~> 2.1.1'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
