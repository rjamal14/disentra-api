module SpiderService
  class NewsListService < Kimurai::Base
    @name = 'news_list_spider'
    @engine = :mechanize

    def self.process(url)
      @start_urls = [url]
      self.crawl!
    end

    def parse(response, url:, data: {})
      url_pages = []
      response.xpath("//div[@class='card']").each do |news|
        url_pages.push("#{ENV['BJB_URL']}#{news.css('a.btn-card-detail').attr('href')&.value}")
      end

      url_pages.each do |page|
        NewsDetailService.process(page)
      end
    end
  end
end
