module SpiderService
  class NewsDetailService < Kimurai::Base
    @name = 'news_detail_spider'
    @engine = :mechanize

    def self.process(url)
      @start_urls = [url]
      self.crawl!
    end

    def parse(response, url:, data: {})
      response.xpath("//div[@class='container-fluid back-articel2']").each do |news|
        item = {}
        image_url = news.css('img.imgdetail-berita').attr('src').value
        fulldate = news.css('span#ContentPlaceHolder1_ctl00_lblTanggal').text.squish.split(' ')
        month = MONTHS.find_all { |month_object| month_object[:id] == fulldate[1] }[0][:number]
        item[:title] = news.css('h2.h2judul-berita').text.squish
        item[:remote_image_url] = "#{ENV['BJB_URL']}/#{image_url.split('../').last}"
        item[:body] = news.css('div.col-md-12')[0].inner_html.squish
        item[:published_at] = DateTime.parse("#{fulldate[2]}-#{month}-#{fulldate[0]}")
        News.create_or_find_by(item)
      end
    end
  end
end
