module Doorkeeper
  class LoginService
    def initialize(params:)
      @params = params
      @result = { user: nil, err_type: nil }
    end

    def perform
      # find user by scope
      user = user_identification_by_scope

      # stop if system didn't find user
      return nil if user.blank?

      # stop if user enter wrong password
      wrong_password_error unless user.try(:valid_password?, @params[:password])

      # update device token
      user.update(device_token: @params[:device_token])

      @result[:user] = user
      # check user has been confirmed/approved
      @result[:err_type] = user_confirmed?(user)
      @result
    end

    # find user by scope
    def user_identification_by_scope
      email = @params[:email]

      if @params[:scope] == 'mobile'
        User.mobile.find_for_database_authentication(email: email)
      else
        User.admin.find_for_database_authentication(email: email)
      end
    end

    # check user has been confirmed/approved and assign error message
    def user_confirmed?(user)
      'not_confirmed' unless user.confirmed_at
    end

    def wrong_password_error
      raise Doorkeeper::Errors::DoorkeeperError, 'wrong_password'
    end
  end
end
