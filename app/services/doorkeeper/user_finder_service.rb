module Doorkeeper
  class UserFinderService
    def initialize(scope:, finder_type:, finder_value:)
      @scope = scope
      @finder_type = finder_type
      @finder_value = finder_value
    end

    def find
      return find_by_scope_email(@scope, @finder_value) if @finder_type == 'email'

      find_by_scope_token(@scope, @finder_value)
    end

    # find user by email
    def find_by_scope_email(scope, email)
      return User.admin.find_by(email: email) if scope == 'admin'

      User.mobile.find_by(email: email)
    end

    # find user by token
    def find_by_scope_token(scope, reset_password_token)
      return User.admin.find_by(reset_password_token: reset_password_token) if scope == 'admin'

      User.mobile.find_by(reset_password_token: reset_password_token)
    end
  end
end
