module Doorkeeper
  class GenerateTokenService
    def initialize(user:)
      @user = user
    end

    def generate
      generated_token = Devise.token_generator.generate(User, :reset_password_token)
      token           = Devise.token_generator.digest(User, :reset_password_token, generated_token)

      @user.update(reset_password_token: token, reset_password_sent_at: DateTime.now)

      UserMailer.reset_password_instructions(@user, @user.reset_password_token).deliver_now
    end
  end
end
