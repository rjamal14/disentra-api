module LearningCenterService
  class YtService
    def initialize(id:, image:)
      @video = Yt::Video.new(id: id)
      @thumbnail = @video.try(:thumbnail_url)
      @image = image.try(:url)
    end

    def get
      {
        view_count: @video.try(:view_count),
        thumbnail: @image.present? ? @image : @thumbnail.gsub(/default.jpg/, 'sddefault.jpg')
      }
    end
  end
end
