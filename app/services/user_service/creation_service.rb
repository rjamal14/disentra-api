module UserService
  class CreationService
    def self.from_omniauth(access_token)
      user = find_or_create_user_from_access_token(access_token)
      return user if user.persisted?

      password = "#{Devise.friendly_token.first(7)}#{rand(10)}"
      user.password = password
      user.password_confirmation = password
      copy_of_user = user.dup
      user.skip_confirmation!
      user.save!
      UserProfile.create(user_id: user.id) unless user.user_profile.present?
      UserAdminMailer.with(user: copy_of_user).password_info_email.deliver_now
      user
    end

    def self.find_or_create_user_from_access_token(access_token)
      User.where(email: access_token.info.email, user_type: 'mobile').first_or_initialize do |user|
        user.name = access_token.info.name
        user.email = access_token.info.email
        user.provider = 'google_oauth2'
        user.user_type = 'mobile'
        user.remote_photo_url = access_token.info.image
      end
    end
  end
end
