# frozen_string_literal: true

class ParticipantSerializer < ActiveModel::Serializer
  attributes :id, :user, :city, :created_at

  def user
    user_attributes = object.try(:user)
    return unless user_attributes

    {
      id: user_attributes.try(:id),
      name: user_attributes.try(:name),
      email: user_attributes.try(:email),
      phone_number: user_attributes.try(:phone_number)
    }
  end

  def city
    city_attributes = object.try(:user).try(:user_profile).try(:city)
    return unless city_attributes

    {
      id: city_attributes.id,
      name: city_attributes.name
    }
  end
end
