# frozen_string_literal: true

class VillageSerializer < ActiveModel::Serializer
  attributes :id, :name, :district_id, :district_name,
             :city_id, :city_name, :created_at, :updated_at

  def district_name
    object.try(:district).try(:name)
  end

  def city_id
    object.try(:district).try(:city_id)
  end

  def city_name
    object.try(:district).try(:city).try(:name)
  end
end
