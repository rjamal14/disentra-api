# frozen_string_literal: true

class BannerSerializer < ActiveModel::Serializer
  attributes :id, :banner_type, :link, :image, :status, :created_by_id,
             :updated_by_id, :created_at, :updated_at
end
