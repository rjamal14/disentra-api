# frozen_string_literal: true

class UmkmMarketSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :business_field, :link,
             :address, :latitude, :longitude, :images, :city,
             :created_by_id, :updated_by_id, :created_at, :updated_at

  def images
    object.try(:umkm_market_images)
  end

  def business_field
    business_field_attributes = object.try(:business_field)

    {
      id: business_field_attributes.try(:id),
      name: business_field_attributes.try(:name)
    }
  end

  def city
    city_attributes = object.try(:city)

    {
      id: city_attributes.try(:id),
      name: city_attributes.try(:name)
    }
  end
end
