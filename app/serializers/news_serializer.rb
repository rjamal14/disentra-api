# frozen_string_literal: true

class NewsSerializer < ActiveModel::Serializer
  attributes :id, :title, :body, :image, :published_at
end
