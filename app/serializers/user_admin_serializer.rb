# frozen_string_literal: true

class UserAdminSerializer < ActiveModel::Serializer
  attributes :id, :email, :user_type, :name, :phone_number, :status,
             :photo, :user_profile, :roles, :super_admin, :business_photos,
             :created_at, :updated_at

  def user_profile
    user_profile = object.try(:user_profile)

    return unless user_profile.present?

    UserProfileAdminSerializer.new(user_profile)
  end

  def business_photos
    object.try(:business_photos)
  end

  def super_admin
    object.super_admin?
  end

  def roles
    object.try(:roles).last
  end
end
