# frozen_string_literal: true

class CitySerializer < ActiveModel::Serializer
  attributes :id, :name, :city_type, :province_id, :province_name,
             :created_at, :updated_at

  def province_name
    object.try(:province).try(:name)
  end
end
