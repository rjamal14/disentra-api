# frozen_string_literal: true

class ConsultantSerializer < ActiveModel::Serializer
  include Formats
  attributes :id, :user_id, :name, :photo, :email, :phone_number, :account_type,
             :consultant_profession, :account_description, :status,
             :created_at, :updated_at

  def name
    object.try(:user).try(:name)
  end

  def photo
    object.try(:user).try(:photo)
  end

  def email
    object.try(:user).try(:email)
  end

  def phone_number
    object.try(:user).try(:phone_number)
  end

  def status
    object.try(:user).try(:status)
  end
end
