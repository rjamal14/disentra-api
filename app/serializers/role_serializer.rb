# frozen_string_literal: true

class RoleSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :role_permissions, :created_at, :updated_at

  def role_permissions
    object.try(:role_permissions)
  end
end
