# frozen_string_literal: true

class BusinessVerificationSerializer < ActiveModel::Serializer
  attributes :id, :checklist_name, :note

  def checklist_name
    object.checklist_verification.name
  end
end
