# frozen_string_literal: true

class UserSerializer < ActiveModel::Serializer
  include Formats
  attributes :id, :email, :user_type, :name, :phone_number, :status,
             :photo, :verified, :verification_status, :trial_expired,
             :user_profile, :roles, :created_at, :updated_at,
             :business_photos

  def user_profile
    user_profile = object.try(:user_profile)

    return unless user_profile.present?

    UserProfileSerializer.new(user_profile)
  end

  def business_photos
    object.try(:business_photos)
  end

  def roles
    object.try(:roles).last
  end

  def trial_expired
    expired = if object.try(:created_at).present?
                object.try(:created_at).to_date + 30.days
              else
                Date.today + 30.days
              end

    return true if DateTime.now > expired

    false
  end
end
