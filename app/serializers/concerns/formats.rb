# frozen_string_literal: true

# class of json serializer foramts
module Formats
  extend ActiveSupport::Concern
  include ActionView::Helpers

  def to_number(value, unit = '')
    return if value.nil?

    number_to_currency(value,
                       unit: unit,
                       separator: ',',
                       delimiter: '.',
                       strip_insignificant_zeros: true)
  end
end
