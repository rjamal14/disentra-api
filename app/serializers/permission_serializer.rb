# frozen_string_literal: true

class PermissionSerializer < ActiveModel::Serializer
  attributes :permission_type, :description, :actions

  def actions
    Permission.where(permission_type: object.try(:permission_type)).select(:id, :action_name)
  end
end
