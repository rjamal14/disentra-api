# frozen_string_literal: true

class BusinessTalkSerializer < ActiveModel::Serializer
  attributes :id, :talk_type, :link, :title, :theme, :event_on, :speaker, :description,
             :start_time, :end_time, :image, :health_protocol, :max_participant, :status,
             :created_by_id, :updated_by_id, :created_at, :updated_at, :participants, :has_joined

  def participants
    participants_attributes = object.try(:participants)

    return unless participants_attributes.present?

    participants_attributes.map { |participant| ParticipantSerializer.new(participant, root: false) }
  end
end
