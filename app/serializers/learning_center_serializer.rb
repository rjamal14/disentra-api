# frozen_string_literal: true

class LearningCenterSerializer < ActiveModel::Serializer
  attributes :id, :content_type, :link, :document, :title, :image,
             :author_name, :description, :status, :additional

  def additional
    return view_count if object.try(:content_type) != 'video'

    LearningCenterService::YtService.new(id: object.try(:link).to_s.split('=')[1], image: object.try(:image)).get
  end

  def view_count
    {
      view_count: object.try(:content_views).count
    }
  end
end
