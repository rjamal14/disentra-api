# frozen_string_literal: true

class UserProfileAdminSerializer < ActiveModel::Serializer
  attributes :id, :nip, :job_position, :account_type,
             :consultant_profession, :account_description

  def job_position
    job_position_attribute = object.try(:job_position)
    {
      id: job_position_attribute.try(:id),
      name: job_position_attribute.try(:name)
    }
  end
end
