# frozen_string_literal: true

class UserProfileSerializer < ActiveModel::Serializer
  include Formats
  attributes :id, :user_id, :account_number, :identity_photo, :whatsapp_number,
             :bussiness_name, :bussiness_description, :bussiness_address,
             :longitude, :latitude, :nip, :job_position, :province_id,
             :province_name, :city_id, :city_name, :district_id, :district_name,
             :village_id, :village_name, :created_at, :updated_at

  def province_name
    object.try(:province).try(:name)
  end

  def city_name
    object.try(:city).try(:name)
  end

  def district_name
    object.try(:district).try(:name)
  end

  def village_name
    object.try(:village).try(:name)
  end

  def job_position
    job_position_attribute = object.try(:job_position)
    {
      id: job_position_attribute.try(:id),
      name: job_position_attribute.try(:name)
    }
  end
end
