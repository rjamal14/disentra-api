# frozen_string_literal: true

class NotificationsSerializer < ActiveModel::Serializer
  include Formats
  attributes :id, :title, :message, :event_type, :action, :has_read, :event,
             :checklist_verification, :created_at, :updated_at

  def event
    object.try(:event)
  end

  def checklist_verification
    return unless object.event_type == 'BusinessVerification' &&
                  object.user.business_verifications.present?

    verifications = object.user.business_verifications
    verifications.map { |verification| BusinessVerificationSerializer.new(verification, root: false) }
  end
end
