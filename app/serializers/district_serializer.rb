# frozen_string_literal: true

class DistrictSerializer < ActiveModel::Serializer
  attributes :id, :name, :city_id, :city_name, :created_at, :updated_at

  def city_name
    object.try(:city).try(:name)
  end
end
