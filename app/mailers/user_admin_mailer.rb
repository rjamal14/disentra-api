class UserAdminMailer < ApplicationMailer
  default from: ENV['MAILER_SENDER']

  def password_info_email
    @user = params[:user]
    mail(to: @user.email, subject: 'DiSentra - Informasi Akun Anda')
  end
end
