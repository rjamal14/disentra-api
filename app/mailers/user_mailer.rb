# frozen_string_literal: true

class UserMailer < Devise::Mailer
  default from: ENV['MAILER_SENDER']
  layout 'mailer'

  def reset_password_instructions(record, token, opts = {})
    @name  = record.name
    @scope = record.user_type
    super
  end

  def account_confirmed
    @user = params[:user]
    mail(to: @user.email, subject: 'DiSentra - Informasi Akun Anda')
  end

  def confirmation_instructions(record, token, opts = {})
    @name = record.name
    super
  end

  protected

  def subject_for(key)
    case key.to_s
    when 'confirmation_instructions'
      'DiSentra - Konfirmasi Akun'
    when 'reset_password_instructions'
      'DiSentra - Ubah Password'
    else
      super
    end
  end
end
