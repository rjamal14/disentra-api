class News < ApplicationRecord
  include PgSearch::Model
  pg_search_scope :search, against: [:title],
                           using: {
                             tsearch: {
                               prefix: true
                             }
                           }

  mount_uploader :image, ImageUploader

  validates :title, presence: true, uniqueness: true

  def self.downz
    tempfile = Down.download('https://www.bankbjb.co.id/images/dynamic/images/artikel/ina/21020027.jpg')
    puts tempfile
  end
end
