# class of checklist verification model
class ChecklistVerification < ApplicationRecord
  has_many :business_verifications

  validates :name, presence: true, uniqueness: true
end
