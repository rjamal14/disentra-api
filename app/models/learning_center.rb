class LearningCenter < ApplicationRecord
  include PgSearch::Model
  pg_search_scope :search, against: %i[title content_type author_name],
                           using: {
                             tsearch: {
                               prefix: true
                             }
                           }
  acts_as_paranoid
  mount_uploader :image, ImageUploader
  mount_uploader :document, PdfUploader

  extend Enumerize
  enumerize :content_type, in: %i[video ebook news]
  enumerize :status, in: %i[show hide], default: :show

  validates_presence_of :content_type, :title, :author_name
  validates :link, presence: true, if: -> { document.blank? }
  validates :link, format: { with: URI::DEFAULT_PARSER.make_regexp(%w[http https]),
                             message: 'URL format is not correct' }, unless: -> { link.blank? }
  validate :video_link_check, if: -> { link.present? && content_type.include?('video') }

  scope :show, -> { where(status: 'show') }
  scope :hide, -> { where(status: 'hide') }

  has_many :content_views, dependent: :destroy

  before_save :update_link, if: -> { document.present? && document_changed? }

  def video_link_check
    return if link =~ /(youtube)/

    errors.add :link, 'video harus youtube URL'
  end

  def update_link
    self.link = document.url
  end
end
