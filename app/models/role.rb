class Role < ApplicationRecord
  include PgSearch::Model
  pg_search_scope :search, against: [:name],
                           using: {
                             tsearch: {
                               prefix: true
                             }
                           }
  acts_as_paranoid

  has_and_belongs_to_many :users, join_table: :users_roles
  has_many :role_permissions
  has_many :permissions, through: :role_permissions

  belongs_to :resource,
             polymorphic: true,
             optional: true

  validates :resource_type,
            inclusion: { in: Rolify.resource_types },
            allow_nil: true

  validates :name, presence: true, uniqueness: true

  scopify

  accepts_nested_attributes_for :role_permissions

  before_update :remove_old_permissions

  private

  def remove_old_permissions
    role_permissions.destroy_all
  end
end
