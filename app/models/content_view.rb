class ContentView < ApplicationRecord
  mount_uploader :image, ImageUploader

  belongs_to :user, optional: true
  belongs_to :learning_center, optional: true

  def viewed?
    self.class.exists?(user_id: user_id, learning_center_id: learning_center_id)
  end
end
