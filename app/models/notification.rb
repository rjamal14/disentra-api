class Notification < ApplicationRecord
  belongs_to :event, polymorphic: true, optional: true
  belongs_to :user

  validates :title, presence: true

  scope :newest, -> { order(created_at: :desc) }

  after_create_commit :push_notification
  before_save :default_values

  private

  def push_notification
    fcm_client = FCM.new(ENV['FCM_SERVER_KEY'])
    options = {
      data: {
        message: message, icon: "#{Rails.root}/public/icon.png"
      },
      notification: {
        body: message,
        title: title,
        sound: 'default',
        icon: "#{Rails.root}/public/icon.png"
      }
    }
    registration_ids = [user.device_token]
    fcm_client.send(registration_ids, options)
  end

  def default_values
    self.has_read ||= false
  end
end
