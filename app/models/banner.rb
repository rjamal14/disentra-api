class Banner < ApplicationRecord
  acts_as_paranoid
  mount_uploader :image, ImageUploader

  extend Enumerize
  enumerize :banner_type, in: %i[home clinic talk learning umkm]
  enumerize :status, in: %i[active inactive], scope: :shallow, default: :active

  validates_presence_of :banner_type, :image
  validate :check_link
  validates :link, format: { with: URI::DEFAULT_PARSER.make_regexp(%w[http https]),
                             message: 'URL tidak sesuai' }, unless: -> { link.blank? }

  scope :home, -> { where(banner_type: 'home') }
  scope :clinic, -> { where(banner_type: 'clinic') }
  scope :talk, -> { where(banner_type: 'talk') }
  scope :learning, -> { where(banner_type: 'learning') }
  scope :umkm, -> { where(banner_type: 'umkm') }

  def check_link
    errors.add :link, 'tidak boleh kosong' if link.blank? && banner_type == 'home'
  end
end
