class District < ApplicationRecord
  include PgSearch::Model
  pg_search_scope :search, against: %i[name],
                           using: {
                             tsearch: {
                               prefix: true
                             }
                           }

  acts_as_paranoid

  has_many :villages

  belongs_to :city

  validates :name, presence: true, uniqueness: true
end
