class UmkmMarket < ApplicationRecord
  include PgSearch::Model
  pg_search_scope :search, against: [:name],
                           using: {
                             tsearch: {
                               prefix: true
                             }
                           }
  acts_as_paranoid

  validates_presence_of :name, :description, :link, :address, :latitude, :longitude, :city_id

  has_many :umkm_market_images, dependent: :destroy, index_errors: true
  belongs_to :city
  belongs_to :business_field

  accepts_nested_attributes_for :umkm_market_images, allow_destroy: true
end
