class Village < ApplicationRecord
  include PgSearch::Model
  pg_search_scope :search, against: %i[name],
                           using: {
                             tsearch: {
                               prefix: true
                             }
                           }

  acts_as_paranoid

  belongs_to :district

  validates :name, presence: true, uniqueness: true
end
