require 'open-uri'

class User < ApplicationRecord
  include PgSearch::Model
  pg_search_scope :search, against: %i[name email],
                           associated_against: {
                             user_profile: :nip
                           },
                           using: {
                             tsearch: {
                               prefix: true
                             }
                           }

  mount_uploader :photo, PhotoUploader

  rolify

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :validatable,
         :omniauthable, omniauth_providers: [:google_oauth2]

  extend Enumerize

  enumerize :user_type, in: %i[admin mobile], scope: :shallow, default: :mobile
  enumerize :status, in: %i[active suspend], default: :active
  enumerize :verification_status, in: %i[basic on_verification approved rejected], default: :basic

  has_one  :user_profile, dependent: :destroy
  has_many :notifications
  has_many :access_grants, -> { where scopes: %w[mobile admin] },
           class_name: 'Doorkeeper::AccessGrant',
           foreign_key: :resource_owner_id,
           dependent: :delete_all
  has_many :access_tokens, -> { where scopes: %w[mobile admin] },
           class_name: 'Doorkeeper::AccessToken',
           foreign_key: :resource_owner_id,
           dependent: :delete_all
  has_many :business_photos, dependent: :destroy
  has_many :business_verifications, dependent: :destroy

  validates :email, uniqueness: true
  validates :password_confirmation, presence: true, on: :create
  validates_presence_of :phone_number
  validates_uniqueness_of :phone_number
  validates_length_of :name, minimum: 3
  validates_length_of :phone_number, maximum: 14

  validate :password_complexity
  validate :phone_number_check

  accepts_nested_attributes_for :user_profile
  accepts_nested_attributes_for :business_photos, allow_destroy: true
  accepts_nested_attributes_for :business_verifications

  scope :admin, -> { where(user_type: 'admin') }
  scope :mobile, -> { where(user_type: 'mobile') }
  scope :active, -> { where(status: 'active') }
  scope :suspended, -> { where(status: 'suspend') }
  scope :on_verification, -> { where(verification_status: 'on_verification') }

  before_save :use_phone_number_format
  after_save :mobile_verification, if: -> { user_type.include?('mobile') && verification_status.include?('basic') }
  before_update :send_verification_information, if: -> { verification_status_changed? }

  def role
    roles.pluck(:name).join(', ').titleize
  end

  def super_admin?
    self.has_role?('Super Admin')
  end

  def admin?
    self.has_role?('Admin')
  end

  def user_umkm?
    self.has_role?('User UMKM')
  end

  def moderator_forum?
    self.has_role?('Moderator Forum')
  end

  def consultant?
    self.has_role?('Konsultan')
  end

  def after_confirmation
    UserMailer.with(user: self).account_confirmed.deliver_now if self.user_type.include?('mobile')
  end

  def use_phone_number_format
    self.phone_number = format_phone_number(self.phone_number)
  end

  def format_phone_number(phone_number)
    phone_number = phone_number.gsub(/^0/, '62') if phone_number.slice(0) == '0'
    phone_number = "62#{phone_number}" if phone_number.slice(0..1) != '62'
    phone_number
  end

  def phone_number_check
    errors.add :phone_number, :taken if self.class.exists?(phone_number: format_phone_number(self.phone_number))
  end

  def password_complexity
    return if password.blank? || password =~ /(?=.*?[0-9])/

    errors.add :password, 'harus mempunyai 1 karakter berupa angka'
  end

  def mobile_verification
    return unless business_profile_data? && business_protos_data?

    self.update(verification_status: 'on_verification')

    self.notifications.build(
      title: 'Verifikasi Bisnis',
      message: 'Data usaha segera ditinjau oleh admin',
      event_type: 'BusinessVerification',
      action: 'verification'
    ).save
  end

  def business_profile_data?
    return unless user_profile.present?

    user_profile.attributes.values_at('account_number', 'identity_photo', 'bussiness_name', 'bussiness_address',
                                      'bussiness_description', 'whatsapp_number').all?(&:present?)
  end

  def business_protos_data?
    business_photos.present?
  end

  def send_verification_information
    send_approved_notification if verification_status.include?('approved')
    send_rejected_notification if verification_status.include?('rejected')
  end

  def send_rejected_notification
    self.notifications.build(
      title: 'Verifikasi Bisnis',
      message: 'Data usaha perlu diperbaiki',
      event_type: 'BusinessVerification',
      action: 'rejected'
    ).save
  end

  def send_approved_notification
    self.verified = true

    self.notifications.build(
      title: 'Verifikasi Bisnis',
      message: 'Data usaha berhasil diverifikasi',
      event_type: 'BusinessVerification',
      action: 'approved'
    ).save
  end
end
