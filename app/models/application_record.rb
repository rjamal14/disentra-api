# frozen_string_literal: true

# Class of Application Record
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
