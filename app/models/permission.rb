# class of permission model
class Permission < ApplicationRecord
  has_many :role_permissions
  has_many :roles, through: :role_permissions

  validates_uniqueness_of :permission_type, scope: [:action_name]
  validates :permission_type, :action_name, presence: true

  scope :action_names, -> { pluck(:action_name).uniq }
  scope :permission_types, -> { pluck(:permission_type).uniq }
end
