class Participant < ApplicationRecord
  belongs_to :event, polymorphic: true
  belongs_to :user

  validates_uniqueness_of :user_id, scope: %i[event_id event_type]
end
