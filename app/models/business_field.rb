class BusinessField < ApplicationRecord
  include PgSearch::Model
  pg_search_scope :search, against: %i[name],
                           using: {
                             tsearch: {
                               prefix: true
                             }
                           }

  acts_as_paranoid

  validates :name, presence: true, uniqueness: true
end
