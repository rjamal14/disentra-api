class Province < ApplicationRecord
  include PgSearch::Model
  pg_search_scope :search, against: [:name],
                           using: {
                             tsearch: {
                               prefix: true
                             }
                           }
  acts_as_paranoid

  has_many :cities

  validates :name, presence: true, uniqueness: true
end
