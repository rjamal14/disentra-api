class City < ApplicationRecord
  include PgSearch::Model
  pg_search_scope :search, against: %i[name],
                           using: {
                             tsearch: {
                               prefix: true
                             }
                           }

  extend Enumerize

  enumerize :city_type, in: %i[city regency], scope: :shallow, default: :city

  acts_as_paranoid

  has_many :districts

  belongs_to :province

  validates :name, presence: true, uniqueness: true
end
