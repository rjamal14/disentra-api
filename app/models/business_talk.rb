class BusinessTalk < ApplicationRecord
  include PgSearch::Model
  pg_search_scope :search, against: %i[title talk_type],
                           using: {
                             tsearch: {
                               prefix: true
                             }
                           }
  attr_accessor :has_joined

  acts_as_paranoid
  mount_uploader :image, ImageUploader

  extend Enumerize
  enumerize :talk_type, in: %i[grupchat sentraminar]
  enumerize :status, in: %i[show hide], default: :show

  has_many :participants, as: :event, dependent: :destroy
  has_many :notifications, as: :event, dependent: :destroy

  scope :show, -> { where(status: 'show') }
  scope :hide, -> { where(status: 'hide') }

  validates_presence_of :talk_type, :link, :title, :theme, :event_on, :speaker, :description
  validates_presence_of :start_time, :end_time, :image, :max_participant
  validates :link, format: { with: URI::DEFAULT_PARSER.make_regexp(%w[http https]),
                             message: 'URL tidak sesuai' }, unless: -> { link.blank? }

  def can_participate?
    max_participant.to_i > participants.count
  end

  def event_not_outdated?
    event_on.to_date >= DateTime.now.to_date
  end
end
