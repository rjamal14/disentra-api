class UserProfile < ApplicationRecord
  mount_uploader :identity_photo, PhotoUploader

  belongs_to :user, optional: true
  belongs_to :job_position, optional: true
  belongs_to :province, optional: true
  belongs_to :city, optional: true
  belongs_to :district, optional: true
  belongs_to :village, optional: true

  validates_length_of :whatsapp_number, minimum: 10, allow_blank: true
  validates_length_of :account_number, minimum: 13, allow_blank: true

  extend Enumerize

  scope :consultant, -> { where("account_type is not null and account_type != ''") }
  scope :user_active, -> { joins(:user).where("users.status ='active'") }

  enumerize :account_type, in: %i[bjb non_bjb], scope: :shallow

  before_save :use_phone_number_format
  before_update :invalid_business_data, if: :verification_pass

  def verification_pass
    user.verification_status.include?('approved') || user.verification_status.include?('rejected')
  end

  def use_phone_number_format
    return unless user.user_type == 'mobile' && self.whatsapp_number.present?

    self.whatsapp_number = format_phone_number(self.whatsapp_number)
  end

  def format_phone_number(whatsapp_number)
    whatsapp_number = whatsapp_number.gsub(/^0/, '62') if whatsapp_number.slice(0) == '0'
    whatsapp_number = "62#{whatsapp_number}" if whatsapp_number.slice(0..1) != '62'
    whatsapp_number
  end

  def invalid_business_data
    return unless business_profile_changes?

    user.update(verification_status: 'basic', verified: false)
    user.business_verifications.destroy_all
  end

  def business_profile_changes?
    self.account_number_changed? || self.identity_photo_changed? ||
      self.bussiness_name_changed? || self.bussiness_address_changed? ||
      self.bussiness_description_changed? || self.whatsapp_number_changed?
  end

  def nip_check
    return unless self.user.user_type == 'admin' && account_type == 'non_bjb'

    errors.add :nip, 'can`t be blank' if nip.blank?
    errors.add :nip, 'already exists' if self.class.exists?(nip: nip)
  end
end
