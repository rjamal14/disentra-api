class UmkmMarketImage < ApplicationRecord
  acts_as_paranoid

  mount_uploader :image, ImageUploader

  validates_presence_of :image

  belongs_to :umkm_market
end
