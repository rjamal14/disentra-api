class PdfUploader < CarrierWave::Uploader::Base
  storage :fog

  def store_dir
    "#{ENV['DO_FOLDER']}/uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def extension_whitelist
    %w[pdf]
  end
end
