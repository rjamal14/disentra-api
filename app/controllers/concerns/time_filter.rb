# frozen_string_literal: true

module TimeFilter
  extend ActiveSupport::Concern
  def use_time_filter(column:)
    params[:q] ||= {}

    case params[:filter_time]
    when 'this_week'
      filter_this_week(column: column)
    when 'this_month'
      filter_this_month(column: column)
    when 'this_year'
      filter_this_year(column: column)
    end
  end

  def filter_this_week(column:)
    params[:q]["#{column}_gteq"] = Date.today.beginning_of_week.try(:strftime, '%Y-%m-%d')
    params[:q]["#{column}_lteq"] = Date.today.end_of_week.try(:strftime, '%Y-%m-%d')
  end

  def filter_this_month(column:)
    params[:q]["#{column}_gteq"] = Date.today.beginning_of_month.try(:strftime, '%Y-%m-%d')
    params[:q]["#{column}_lteq"] = Date.today.end_of_month.try(:strftime, '%Y-%m-%d')
  end

  def filter_this_year(column:)
    params[:q]["#{column}_gteq"] = Date.today.beginning_of_year.try(:strftime, '%Y-%m-%d')
    params[:q]["#{column}_lteq"] = Date.today.end_of_year.try(:strftime, '%Y-%m-%d')
  end
end
