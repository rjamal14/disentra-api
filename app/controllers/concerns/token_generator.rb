module TokenGenerator
  def create_token(id)
    Doorkeeper::AccessToken.create!(
      application_id: 2,
      resource_owner_id: id,
      expires_in: 2.hours,
      scopes: 'mobile'
    )
  end

  def respons_token(user:)
    token = create_token(user.id)
    render json: {
      access_token: token.token,
      token_type: 'Bearer',
      expires_in: token.expires_in,
      scope: token.scopes,
      created_at: token.created_at,
      data: UserSerializer.new(user)
    }, status: 200
  end
end
