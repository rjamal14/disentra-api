# frozen_string_literal: true

# Class of Admin Record
class AdminController < ApplicationController
  before_action -> { doorkeeper_authorize! :admin }
  before_action :current_admin
  authorize_resource if: :json_request?

  def current_admin
    return if doorkeeper_token.nil?

    User.admin.where(id: doorkeeper_token.resource_owner_id).last
  end

  def current_ability
    @current_ability ||= Ability.new(current_admin)
  end

  def json_request?
    request.format.json?
  end

  rescue_from CanCan::AccessDenied do |exception|
    response_error('Unauthorized', exception.message, 401)
  end
end
