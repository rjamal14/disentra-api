# frozen_string_literal: true

class TokensController < Doorkeeper::TokensController
  def revoke
    unless server.client
      render json: revocation_error_response, status: :forbidden
      return
    end

    if token.blank?
      render json: {}, status: 200
    elsif authorized?
      revoke_token

      render json: {}, status: 200
    else
      render json: revocation_error_response, status: :forbidden
    end
  end

  private

  def owner_id
    Doorkeeper::AccessToken.where(token: params[:token])
                           .try(:last).try(:resource_owner_id)
  end
end
