# frozen_string_literal: true

class ApplicationController < ActionController::API
  include Response
  include ExceptionHandler
  include SerializerGenerator
  include TokenGenerator

  respond_to :json

  def current_admin
    return if doorkeeper_token.nil?

    User.admin.where(id: doorkeeper_token.resource_owner_id).last
  end

  def current_mobile
    return if doorkeeper_token.nil?

    User.mobile.where(id: doorkeeper_token.resource_owner_id).last
  end

  def current_user
    return if doorkeeper_token.nil?

    User.find(doorkeeper_token.resource_owner_id)
  end
end
