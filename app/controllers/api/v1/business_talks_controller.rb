# frozen_string_literal: true

module Api
  module V1
    # BusinessTalksController controller for user
    class BusinessTalksController < AdminController
      before_action :set_business_talk, only: %i[show update destroy]
      before_action :search_business_talk, only: :index

      def index
        @business_talks = @business_talks.page(params[:page] || 1).per(params[:per_page] || 10)
                                         .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

        serial_business_talk = @business_talks.map { |business_talk| BusinessTalkSerializer.new(business_talk, root: false) }

        response_pagination(serial_business_talk, @business_talks, 'Daftar Bincang Bisnis')
      end

      def show
        response_success('Data Bincang Bisnis', BusinessTalkSerializer.new(@business_talk))
      end

      def create
        business_talk = BusinessTalk.new(business_talk_params.merge(created_by_id: current_admin.id))

        if business_talk.save
          response_success('Tambah Bincang Bisnis berhasil', BusinessTalkSerializer.new(business_talk), 201)
        else
          response_error('Tambah Bincang Bisnis gagal', business_talk.errors.full_messages)
        end
      end

      def update
        business_talk_attribute = business_talk_params.merge(updated_by_id: current_admin.id)
        if @business_talk.update(business_talk_attribute)
          response_success('Ubah Bincang Bisnis berhasil', BusinessTalkSerializer.new(@business_talk))
        else
          response_error('Ubah Bincang Bisnis gagal', @business_talk.errors.full_messages)
        end
      end

      def destroy
        return unless @business_talk.destroy

        response_success('Hapus Bincang Bisnis berhasil')
        @business_talk.update(deleted_by_id: current_admin.id)
      end

      private

      def search_business_talk
        @business_talks = if params[:search].present? && params[:search] != '{search}'
                            BusinessTalk.search(params[:search])
                          else
                            BusinessTalk
                          end

        @business_talks = @business_talks.ransack(params[:q]).result
      end

      def set_business_talk
        @business_talk = BusinessTalk.find(params[:id])
      rescue StandardError
        response_error('Eror Bincang Bisnis', 'Data tidak ditemukan')
      end

      def business_talk_params
        params.fetch(:business_talk, {}).permit(:talk_type, :link, :title, :theme, :event_on,
                                                :speaker, :description, :status, :image,
                                                :start_time, :end_time, :health_protocol,
                                                :max_participant)
      end
    end
  end
end
