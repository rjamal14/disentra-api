# frozen_string_literal: true

module Api
  module V1
    # ChecklistVerificationsController controller for checklist_verification
    class ChecklistVerificationsController < AdminController
      def index
        @checklist_verifications = ChecklistVerification.all.order('created_at ASC')

        response_success('Daftar Checklist Verifikasi', @checklist_verifications)
      end
    end
  end
end
