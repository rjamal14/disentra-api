# frozen_string_literal: true

module Api
  module V1
    # Controller for manage user data
    class UserController < ApplicationController
      before_action -> { doorkeeper_authorize! :mobile, :admin }

      def show
        response_success('Data User', UserSerializer.new(current_user))
      end

      def update
        if current_user.update!(user_params)
          response_success('Ubah data User berhasil', UserSerializer.new(current_user))
        else
          response_error('Ubah data User gagal', current_user.errors.full_messages)
        end
      end

      def update_password
        if current_user.update_with_password(password_params)
          response_success('Ubah data User berhasil', UserSerializer.new(current_user))
        else
          response_error('Ubah data User gagal', current_user.errors)
        end
      end

      private

      def user_params
        user_profile_attributes = %i[id account_number identity_photo bussiness_name bussiness_address
                                     bussiness_description whatsapp_number longitude latitude province_id
                                     city_id district_id village_id]
        params.fetch(:user, {})
              .permit(:name, :phone_number, :photo,
                      user_profile_attributes: user_profile_attributes,
                      business_photos_attributes: %i[id image _destroy])
      end

      def password_params
        params.required(:user).permit(:password, :password_confirmation, :current_password)
      end
    end
  end
end
