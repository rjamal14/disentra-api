# frozen_string_literal: true

module Api
  module V1
    # UserUmkmsController controller for user
    class UserUmkmsController < AdminController
      before_action :set_user_umkm, only: %i[show update business_verification_list]
      before_action :search_users, only: :index

      def index
        @user_umkms = @user_umkms.page(params[:page] || 1).per(params[:per_page] || 10)
                                 .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_user_umkm = @user_umkms.map { |user_umkm| UserSerializer.new(user_umkm, root: false) }

        response_pagination(serial_user_umkm, @user_umkms, 'Daftar UMKM')
      end

      def show
        response_success('Data UMKM', UserSerializer.new(@user_umkm))
      end

      def update
        if @user_umkm.update(user_umkm_params)
          response_success('Ubah UMKM berhasil', UserSerializer.new(@user_umkm))
        else
          response_error('Ubah UMKM gagal', @user_umkm.errors.full_messages)
        end
      end

      def business_verification_list
        business_verifications = @user_umkm.business_verifications

        response_success('Daftar Verifikasi Bisnis', business_verifications)
      end

      def business_verification_counter
        total_on_verification = User.mobile.on_verification.count

        verification_counter = { total_on_verification: total_on_verification }

        response_success('Total Verifikasi Bisnis', verification_counter)
      end

      private

      def search_users
        @user_umkms = if params[:search].present? && params[:search] != '{search}'
                        User.mobile.search(params[:search])
                      else
                        User.mobile
                      end
        @user_umkms = @user_umkms.with_role(params[:role_name]) if params[:role_name].present?
        @user_umkms = @user_umkms.ransack(params[:q]).result
      end

      def set_user_umkm
        @user_umkm = User.mobile.find(params[:id])
      rescue StandardError
        response_error('Eror UMKM', 'Data tidak ditemukan')
      end

      def user_umkm_params
        params.fetch(:user, {}).permit(:status, :verification_status,
                                       business_verifications_attributes: %i[
                                         checklist_verification_id note
                                       ])
      end
    end
  end
end
