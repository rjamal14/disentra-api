# frozen_string_literal: true

module Api
  module V1
    # VillagesController controller for user
    class VillagesController < AdminController
      before_action :set_village, only: %i[show update destroy]
      before_action :search_village, only: :index

      def index
        @villages      = @villages.page(params[:page] || 1).per(params[:per_page] || 10)
                                  .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_village = @villages.map { |village| VillageSerializer.new(village, root: false) }

        response_pagination(serial_village, @villages, 'Daftar Kelurahan')
      end

      def show
        response_success('Data Kelurahan', VillageSerializer.new(@village))
      end

      def create
        village = Village.new(village_params.merge(created_by_id: current_admin.id))

        if village.save
          response_success('Tambah Kelurahan berhasil', VillageSerializer.new(village), 201)
        else
          response_error('Tambah Kelurahan gagal', village.errors.full_messages)
        end
      end

      def update
        village_attribute = village_params.merge(updated_by_id: current_admin.id)
        if @village.update(village_attribute)
          response_success('Ubah Kelurahan berhasil', VillageSerializer.new(@village))
        else
          response_error('Ubah Kelurahan gagal', @village.errors.full_messages)
        end
      end

      def destroy
        return unless @village.destroy

        response_success('Hapus Kelurahan berhasil')
        @village.update(deleted_by_id: current_admin.id)
      end

      private

      def search_village
        @villages = if params[:search].present? && params[:search] != '{search}'
                      Village.search(params[:search])
                    else
                      Village
                    end
        @villages = @villages.ransack(params[:q]).result
      end

      def set_village
        @village = Village.find(params[:id])
      rescue StandardError
        response_error('Eror Kelurahan', 'Data tidak ditemukan')
      end

      def village_params
        params.fetch(:village, {}).permit(:name, :district_id)
      end
    end
  end
end
