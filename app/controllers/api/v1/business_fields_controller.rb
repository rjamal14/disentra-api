# frozen_string_literal: true

module Api
  module V1
    # BusinessFieldsController controller for user
    class BusinessFieldsController < AdminController
      before_action :set_business_field, only: %i[show update destroy]
      before_action :search_business_field, only: :index

      def index
        @business_fields = @business_fields.page(params[:page] || 1).per(params[:per_page] || 10)
                                           .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        business_fields  = @business_fields.map { |business_field| BusinessFieldSerializer.new(business_field, root: false) }

        response_pagination(business_fields, @business_fields, 'Daftar Bidang Usaha')
      end

      def all_business_field
        @business_fields = BusinessField.order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        business_fields  = @business_fields.map { |business_field| BusinessFieldSerializer.new(business_field, root: false) }

        response_success('Daftar Bidang Usaha', business_fields)
      end

      def show
        response_success('Data Bidang Usaha', BusinessFieldSerializer.new(@business_field))
      end

      def create
        business_field = BusinessField.new(business_field_params.merge(created_by_id: current_admin.id))

        if business_field.save
          response_success('Tambah Bidang Usaha berhasil', BusinessFieldSerializer.new(business_field), 201)
        else
          response_error('Tambah Bidang Usaha gagal', business_field.errors.full_messages)
        end
      end

      def update
        business_field_attribute = business_field_params.merge(updated_by_id: current_admin.id)
        if @business_field.update(business_field_attribute)
          response_success('Ubah Bidang Usaha berhasil', BusinessFieldSerializer.new(@business_field))
        else
          response_error('Ubah Bidang Usaha gagal', @business_field.errors.full_messages)
        end
      end

      def destroy
        return unless @business_field.destroy

        response_success('Hapus Bidang Usaha berhasil')
        @business_field.update(deleted_by_id: current_admin.id)
      end

      private

      def search_business_field
        @business_fields = if params[:search].present? && params[:search] != '{search}'
                             BusinessField.search(params[:search])
                           else
                             BusinessField
                           end
        @business_fields = @business_fields.ransack(params[:q]).result
      end

      def set_business_field
        @business_field = BusinessField.find(params[:id])
      rescue StandardError
        response_error('Eror Bidang Usaha', 'Data tidak ditemukan')
      end

      def business_field_params
        params.fetch(:business_field, {}).permit(:name)
      end
    end
  end
end
