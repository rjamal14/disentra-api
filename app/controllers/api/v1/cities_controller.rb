# frozen_string_literal: true

module Api
  module V1
    # CitiesController controller for user
    class CitiesController < AdminController
      before_action :set_city, only: %i[show update destroy]
      before_action :search_city, only: :index

      def index
        @cities     = @cities.page(params[:page] || 1).per(params[:per_page] || 10)
                             .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_city = @cities.map { |city| CitySerializer.new(city, root: false) }

        response_pagination(serial_city, @cities, 'Daftar Kota/Kabupaten')
      end

      def show
        response_success('Data Kota/Kabupaten', CitySerializer.new(@city))
      end

      def create
        city = City.new(city_params.merge(created_by_id: current_admin.id))

        if city.save
          response_success('Tambah Kota/Kabupaten berhasil', CitySerializer.new(city), 201)
        else
          response_error('Tambah Kota/Kabupaten gagal', city.errors.full_messages)
        end
      end

      def update
        city_attribute = city_params.merge(updated_by_id: current_admin.id)
        if @city.update(city_attribute)
          response_success('Ubah Kota/Kabupaten berhasil', CitySerializer.new(@city))
        else
          response_error('Ubah Kota/Kabupaten gagal', @city.errors.full_messages)
        end
      end

      def destroy
        return unless @city.destroy

        response_success('Hapus Kota/Kabupaten berhasil')
        @city.update(deleted_by_id: current_admin.id)
      end

      private

      def search_city
        @cities = if params[:search].present? && params[:search] != '{search}'
                    City.search(params[:search])
                  else
                    City
                  end
        @cities = @cities.ransack(params[:q]).result
      end

      def set_city
        @city = City.find(params[:id])
      rescue StandardError
        response_error('Eror Kota/Kabupaten', 'Data tidak ditemukan')
      end

      def city_params
        params.fetch(:city, {}).permit(:name, :city_type, :province_id)
      end
    end
  end
end
