# frozen_string_literal: true

module Api
  module V1
    module Users
      # registration controller for user
      class RegistrationsController < Devise::RegistrationsController
        def create
          build_resource(sign_up_params)
          if resource.save
            response_success('Registrasi Sukses', UserSerializer.new(resource))
          else
            response_error('Registrasi Gagal', resource.errors.full_messages)
          end
        end

        private

        def sign_up_params
          params.fetch(:user, {})
                .permit(:name, :email, :password, :user_type, :phone_number,
                        :device_token, :password_confirmation)
        end
      end
    end
  end
end
