# app/controllers/users/omniauth_callbacks_controller.rb:
# frozen_string_literal: true

module Api
  module V1
    module Users
      class OmniauthCallbacksController < Devise::OmniauthCallbacksController
        def google_oauth2
          @user = UserService::CreationService.from_omniauth(request.env['omniauth.auth'])
          respons_token(user: @user)
        end
      end
    end
  end
end
