# frozen_string_literal: true

module Api
  module V1
    module Users
      # controller class v1 for saas user confirmation
      class ConfirmationsController < Devise::ConfirmationsController
        # GET /resource/confirmation?confirmation_token=abcdef
        def show
          self.resource = resource_class.confirm_by_token(params[:confirmation_token])
          yield resource if block_given?

          confirmation_account_activity(resource)
        end

        private

        def confirmation_account_activity(resource)
          if resource.errors.empty?
            response_success('Konfirmasi Akun Sukses', UserSerializer.new(resource).attributes)
          else
            response_error('Konfirmasi Akun Gagal', resource.errors.full_messages)
          end
        end
      end
    end
  end
end
