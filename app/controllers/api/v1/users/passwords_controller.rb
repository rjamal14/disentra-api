# frozen_string_literal: true

module Api
  module V1
    module Users
      # controller class v1 for user password
      class PasswordsController < Devise::PasswordsController
        def create
          return response_error('Permintaan reset Kata Sandi gagal', 'E-mail tidak boleh kosong') if params['email'].blank?

          user = Doorkeeper::UserFinderService.new(scope: params['scope'], finder_type: 'email',
                                                   finder_value: params['email']).find

          generate_token(user)
        end

        def edit
          reset_password_token = params['reset_password_token']

          resource = Doorkeeper::UserFinderService.new(scope: params['scope'], finder_type: 'token',
                                                       finder_value: reset_password_token).find

          return response_error('Permintaan reset Kata Sandi gagal', 'Token Reset Kata Sandi salah') unless resource.present?

          response_success('Permintaan reset Kata Sandi berhasil', { token: params[:reset_password_token],
                                                                     user: UserSerializer.new(resource) })
        end

        # PUT /resource/password
        def update
          reset_password_token = params['reset_password_token']

          resource = Doorkeeper::UserFinderService.new(scope: params['scope'], finder_type: 'token',
                                                       finder_value: reset_password_token).find

          return response_error('Reset Kata Sandi gagal', 'Token Reset Kata Sandi salah') unless resource.present?

          reset_password_activity(resource)
        end

        protected

        def generate_token(user)
          if user.present?
            if user.confirmed_at.blank?
              return response_error('Permintaan reset Kata Sandi gagal', 'Akun User belum terkonfirmasi')
            end

            Doorkeeper::GenerateTokenService.new(user: user).generate

            response_success('Permintaan reset Kata Sandi berhasil')
          else
            response_error('Permintaan reset Kata Sandi gagal', 'E-mail tidak ditemukan')
          end
        end

        def reset_password_activity(resource)
          if resource.reset_password(reset_password_params[:password],
                                     reset_password_params[:password_confirmation])
            response_success('Reset Kata Sandi berhasil')
          else
            response_error('Reset Kata Sandi gagal', resource.errors.full_messages)
          end
        end

        def reset_password_params
          params.fetch(:user, {})
                .permit(:password, :password_confirmation)
        end
      end
    end
  end
end
