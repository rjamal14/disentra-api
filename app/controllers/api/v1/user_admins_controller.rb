# frozen_string_literal: true

module Api
  module V1
    # UserAdminsController controller for user
    class UserAdminsController < AdminController
      before_action :set_user_admin, only: %i[show update]
      before_action :search_user_admin, only: :index
      skip_authorize_resource only: %i[show update]

      def index
        @user_admins = @user_admins.page(params[:page] || 1).per(params[:per_page] || 10)
                                   .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_user_admin = @user_admins.map { |user_admin| UserAdminSerializer.new(user_admin, root: false) }

        response_pagination(serial_user_admin, @user_admins, 'Daftar Admin')
      end

      def show
        response_success('Data Admin', UserAdminSerializer.new(@user_admin))
      end

      def create
        password = "#{Devise.friendly_token.first(7)}#{rand(10)}"
        user_admin = User.admin.new(user_admin_params.merge(password: password,
                                                            password_confirmation: password))
        user_info_attributes = user_admin
        user_admin.skip_confirmation!
        if user_admin.save
          UserAdminMailer.with(user: user_info_attributes).password_info_email.deliver_now
          response_success('Tambah Data Admin berhasil', UserAdminSerializer.new(user_admin), 201)
        else
          response_error('Tambah Data Admin gagal', user_admin.errors.full_messages)
        end
      end

      def update
        if @user_admin.update(user_admin_params)
          response_success('Ubah Data Admin berhasil', UserAdminSerializer.new(@user_admin))
        else
          response_error('Ubah Data Admin gagal', @user_admin.errors.full_messages)
        end
      end

      private

      def search_user_admin
        @user_admins = if params[:search].present? && params[:search] != '{search}'
                         User.admin.search(params[:search])
                       else
                         User.admin
                       end
        @user_admins = @user_admins.with_role(params[:role_name]) if params[:role_name].present?
        @user_admins = @user_admins.ransack(params[:q]).result
      end

      def set_user_admin
        @user_admin = User.admin.find(params[:id])
      rescue StandardError
        response_error('Eror Admin', 'Data tidak ditemukan')
      end

      def user_admin_params
        params.fetch(:user, {}).permit(:name, :email, :phone_number, :photo, :status, { role_ids: [] },
                                       user_profile_attributes: %i[id nip job_position_id account_type
                                                                   consultant_profession
                                                                   account_description])
      end
    end
  end
end
