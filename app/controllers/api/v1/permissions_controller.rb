# frozen_string_literal: true

module Api
  module V1
    # PermissionsController controller for permission
    class PermissionsController < AdminController
      def index
        @permissions = Permission.select(:permission_type, :description).distinct(true)

        serial_permission = @permissions.map { |permission| PermissionSerializer.new(permission, root: false) }

        response_success('Daftar Permission', serial_permission)
      end
    end
  end
end
