# frozen_string_literal: true

module Api
  module V1
    # LearningCentersController controller for user
    class LearningCentersController < AdminController
      before_action :set_learning_center, only: %i[show update destroy]
      before_action :search_learning_center, only: :index

      def index
        @learning_centers = @learning_centers.page(params[:page] || 1).per(params[:per_page] || 10)
                                             .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

        serial_learning_center = @learning_centers.map do |learning_center|
          LearningCenterSerializer.new(learning_center, root: false)
        end

        response_pagination(serial_learning_center, @learning_centers, 'Daftar Sentra Belajar')
      end

      def show
        response_success('Data Sentra Belajar', LearningCenterSerializer.new(@learning_center))
      end

      def create
        learning_center = LearningCenter.new(learning_center_params.merge(created_by_id: current_admin.id))

        if learning_center.save
          response_success('Tambah Sentra Belajar berhasil', LearningCenterSerializer.new(learning_center), 201)
        else
          response_error('Tambah Sentra Belajar gagal', learning_center.errors.full_messages)
        end
      end

      def update
        learning_center_attribute = learning_center_params.merge(updated_by_id: current_admin.id)
        if @learning_center.update(learning_center_attribute)
          response_success('Ubah Sentra Belajar berhasil', LearningCenterSerializer.new(@learning_center))
        else
          response_error('Ubah Sentra Belajar gagal', @learning_center.errors.full_messages)
        end
      end

      def destroy
        return unless @learning_center.destroy

        response_success('Hapus Sentra Belajar berhasil')
        @learning_center.update(deleted_by_id: current_admin.id)
      end

      private

      def search_learning_center
        @learning_centers = if params[:search].present? && params[:search] != '{search}'
                              LearningCenter.search(params[:search])
                            else
                              LearningCenter
                            end

        @learning_centers = @learning_centers.ransack(params[:q]).result
      end

      def set_learning_center
        @learning_center = LearningCenter.find(params[:id])
      rescue StandardError
        response_error('Eror Sentra Belajar', 'Data tidak ditemukan')
      end

      def learning_center_params
        params.fetch(:learning_center, {}).permit(:content_type, :link, :title, :image, :author_name,
                                                  :document, :description, :status)
      end
    end
  end
end
