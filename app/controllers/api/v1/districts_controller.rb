# frozen_string_literal: true

module Api
  module V1
    # DistrictsController controller for user
    class DistrictsController < AdminController
      before_action :set_district, only: %i[show update destroy]
      before_action :search_district, only: :index

      def index
        @districts      = @districts.page(params[:page] || 1).per(params[:per_page] || 10)
                                    .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_district = @districts.map { |district| DistrictSerializer.new(district, root: false) }

        response_pagination(serial_district, @districts, 'Daftar Kecamatan')
      end

      def show
        response_success('Data Kecamatan', DistrictSerializer.new(@district))
      end

      def create
        district = District.new(district_params.merge(created_by_id: current_admin.id))

        if district.save
          response_success('Tambah Kecamatan berhasil', DistrictSerializer.new(district), 201)
        else
          response_error('Tambah Kecamatan gagal', district.errors.full_messages)
        end
      end

      def update
        district_attribute = district_params.merge(updated_by_id: current_admin.id)
        if @district.update(district_attribute)
          response_success('Ubah Kecamatan berhasil', DistrictSerializer.new(@district))
        else
          response_error('Ubah Kecamatan gagal', @district.errors.full_messages)
        end
      end

      def destroy
        return unless @district.destroy

        response_success('Hapus Kecamatan berhasil')
        @district.update(deleted_by_id: current_admin.id)
      end

      private

      def search_district
        @districts = if params[:search].present? && params[:search] != '{search}'
                       District.search(params[:search])
                     else
                       District
                     end
        @districts = @districts.ransack(params[:q]).result
      end

      def set_district
        @district = District.find(params[:id])
      rescue StandardError
        response_error('Eror Kecamatan', 'Data tidak ditemukan')
      end

      def district_params
        params.fetch(:district, {}).permit(:name, :city_id)
      end
    end
  end
end
