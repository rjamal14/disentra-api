# frozen_string_literal: true

module Api
  module V1
    # controller class v1 for user sessions
    class SessionsController < Devise::SessionsController
    end
  end
end
