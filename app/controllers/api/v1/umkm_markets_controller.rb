# frozen_string_literal: true

module Api
  module V1
    class UmkmMarketsController < AdminController
      before_action :set_umkm_market, only: %i[show update destroy]
      before_action :search_umkm_market, only: :index

      def index
        @umkm_markets = @umkm_markets.page(params[:page] || 1).per(params[:per_page] || 10)
                                     .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

        serial_umkm_market = @umkm_markets.map { |umkm_market| UmkmMarketSerializer.new(umkm_market, root: false) }

        response_pagination(serial_umkm_market, @umkm_markets, 'Daftar Pasar UMKM')
      end

      def show
        response_success('Data Pasar UMKM', UmkmMarketSerializer.new(@umkm_market))
      end

      def create
        umkm_market = UmkmMarket.new(umkm_market_params.merge(created_by_id: current_admin.id))

        if umkm_market.save
          response_success('Tambah Pasar UMKM berhasil', UmkmMarketSerializer.new(umkm_market), 201)
        else
          response_error('Tambah Pasar UMKM gagal', umkm_market.errors.full_messages)
        end
      end

      def update
        umkm_market_attribute = umkm_market_params.merge(updated_by_id: current_admin.id)
        if @umkm_market.update(umkm_market_attribute)
          response_success('Ubah Pasar UMKM berhasil', UmkmMarketSerializer.new(@umkm_market))
        else
          response_error('Ubah Pasar UMKM gagal', @umkm_market.errors.full_messages)
        end
      end

      def destroy
        return unless @umkm_market.destroy

        response_success('Hapus Pasar UMKM berhasil')
        @umkm_market.update(deleted_by_id: current_admin.id)
      end

      private

      def search_umkm_market
        @umkm_markets = if params[:search].present?
                          UmkmMarket.search(params[:search])
                        else
                          UmkmMarket
                        end

        @umkm_markets = @umkm_markets.ransack(params[:q]).result
      end

      def set_umkm_market
        @umkm_market = UmkmMarket.find(params[:id])
      rescue StandardError
        response_error('Eror Pasar UMKM', 'Data tidak ditemukan')
      end

      def umkm_market_params
        params.fetch(:umkm_market, {}).permit(:name, :description, :business_field_id,
                                              :latitude, :longitude, :link, :address, :city_id,
                                              umkm_market_images_attributes: %i[id image is_thumbnail _destroy])
      end
    end
  end
end
