# frozen_string_literal: true

module Api
  module V1
    # RolesController controller for role
    class RolesController < AdminController
      before_action :set_role, only: %i[show update destroy]
      before_action :search_role, only: %i[index all_role]
      skip_authorize_resource only: :all_role

      def index
        @roles      = @roles.page(params[:page] || 1).per(params[:per_page] || 10)
                            .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_role = @roles.map { |role| RoleIndexSerializer.new(role, root: false) }

        response_pagination(serial_role, @roles, 'Daftar Role')
      end

      def all_role
        @roles      = @roles.order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_role = @roles.map { |role| RoleIndexSerializer.new(role, root: false) }

        response_success('Daftar Role', serial_role)
      end

      def show
        response_success('Data Role', RoleSerializer.new(@role))
      end

      def create
        role = Role.new(role_params)

        if role.save
          response_success('Tambah Role berhasil', RoleSerializer.new(role), 201)
        else
          response_error('Tambah Role gagal', role.errors.full_messages)
        end
      end

      def update
        permissions = role_params[:role_permissions_attributes]

        if @role.update(role_params)
          refresh_permissions(permissions) if permissions.present?
          response_success('Ubah Role berhasil', RoleSerializer.new(@role))
        else
          response_error('Ubah Role berhasil', @role.errors.full_messages)
        end
      end

      def destroy
        return unless @role.destroy

        response_success('Hapus Role berhasil')
        @role.update(deleted_by_id: current_admin.id)
      end

      private

      def search_role
        @roles = if params[:search].present? && params[:search] != '{search}'
                   Role.search(params[:search])
                 else
                   Role
                 end

        @roles = @roles.ransack(params[:q]).result
      end

      def refresh_permissions(permissions)
        permissions.each do |permission|
          permission_id = permission[:permission_id]
          RolePermission.create(role_id: @role.id, permission_id: permission_id)
        end
      end

      def set_role
        @role = Role.find(params[:id])
      rescue StandardError
        response_error('Eror Role', 'Data tidak ditemukan')
      end

      def role_params
        params.fetch(:role, {}).permit(:name, :description,
                                       role_permissions_attributes: %i[permission_id])
      end
    end
  end
end
