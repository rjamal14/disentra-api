# frozen_string_literal: true

module Api
  module V1
    module Mobile
      # BusinessClinicsController controller for user
      class BusinessClinicsController < MobileController
        def index
          @business_clinics = UserProfile.consultant.user_active
          @business_clinics = @business_clinics.page(params[:page] || 1).per(params[:per_page] || 10)
                                               .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
          business_clinics  = @business_clinics.map do |business_clinic|
            ConsultantSerializer.new(business_clinic, root: false)
          end

          response_pagination(business_clinics, @business_clinics, 'Daftar Klinik Bisnis')
        end
      end
    end
  end
end
