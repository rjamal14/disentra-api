# frozen_string_literal: true

module Api
  module V1
    module Mobile
      # BusinessFieldsController controller for user
      class BusinessFieldsController < BaseController
        def index
          @business_fields = BusinessField.ransack(params[:q]).result
          @business_fields = @business_fields.order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
          business_fields  = @business_fields.map do |business_field|
            BusinessFieldSerializer.new(business_field, root: false)
          end

          response_success('Daftar Bidang Usaha', business_fields)
        end
      end
    end
  end
end
