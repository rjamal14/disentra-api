# frozen_string_literal: true

module Api
  module V1
    module Mobile
      # CitiesController controller for user
      class CitiesController < BaseController
        def index
          @cities     = City.ransack(params[:q]).result
          @cities     = @cities.order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
          serial_city = @cities.map { |city| CitySerializer.new(city, root: false) }

          response_success('Daftar Kota/Kabupaten', serial_city)
        end

        def search_index
          @cities     = City.ransack(params[:q]).result
          @cities     = @cities.page(params[:page] || 1).per(params[:per_page] || 15)
                               .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
          serial_city = @cities.map { |city| CitySerializer.new(city, root: false) }

          response_pagination(serial_city, @cities, 'Daftar Kota/Kabupaten')
        end
      end
    end
  end
end
