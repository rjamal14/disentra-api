# frozen_string_literal: true

module Api
  module V1
    module Mobile
      # NotificationsController controller for user notification
      class NotificationsController < MobileController
        before_action :set_notification, only: :show

        def index
          @notifications = Notification.where(user_id: current_mobile.id).newest
                                       .page(params[:page] || 1).per(params[:per_page] || 10)

          serial_notifications = @notifications.map do |notification|
            NotificationsSerializer.new(notification, root: false)
          end

          response_pagination(serial_notifications, @notifications, 'Notifikasi User')
        end

        def show
          response_success('Detail Notifikasi', NotificationsSerializer.new(@notification))
        end

        def notifications_count
          notifications = Notification.where(user_id: current_mobile.id, has_read: false)
          response_each_serializer('Total Notifikasi', { count: notifications.count })
        end

        def read_all_notifications
          notifications = Notification.where(user_id: current_mobile.id)
          notifications.update_all(has_read: true)
          response_each_serializer('Tandai semua notifikasi telah dibaca', notifications, NotificationsSerializer)
        end

        private

        def set_notification
          @notification = Notification.where(user_id: current_mobile.id).find(params[:id])
          @notification.update(has_read: true)
        rescue StandardError
          response_error('Eror Notifikasi', 'Data tidak ditemukan')
        end
      end
    end
  end
end
