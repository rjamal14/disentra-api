# frozen_string_literal: true

module Api
  module V1
    module Mobile
      class UmkmMarketsController < MobileController
        before_action :set_umkm_market, only: :show
        before_action :search_umkm_market, only: :index

        def index
          @umkm_markets = @umkm_markets.page(params[:page] || 1).per(params[:per_page] || 10)
                                       .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

          serial_umkm_market = @umkm_markets.map { |umkm_market| UmkmMarketSerializer.new(umkm_market, root: false) }

          response_pagination(serial_umkm_market, @umkm_markets, 'Daftar Pasar UMKM')
        end

        def show
          response_success('Data Pasar UMKM', UmkmMarketSerializer.new(@umkm_market))
        end

        private

        def search_umkm_market
          @umkm_markets = if params[:search].present?
                            UmkmMarket.search(params[:search])
                          else
                            UmkmMarket
                          end

          @umkm_markets = @umkm_markets.ransack(params[:q]).result
        end

        def set_umkm_market
          @umkm_market = UmkmMarket.find(params[:id])
        rescue StandardError
          response_error('Eror Pasar UMKM', 'Data tidak ditemukan')
        end
      end
    end
  end
end
