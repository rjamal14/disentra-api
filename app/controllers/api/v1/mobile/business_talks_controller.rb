# frozen_string_literal: true

module Api
  module V1
    module Mobile
      # BusinessTalksController controller for user mobile
      class BusinessTalksController < MobileController
        include TimeFilter

        before_action :set_business_talk, only: %i[show create_participant]
        before_action :search_business_talk, only: :index

        def index
          @business_talks = @business_talks.page(params[:page] || 1).per(params[:per_page] || 10)
                                           .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

          serial_business_talk = @business_talks.map do |business_talk|
            business_talk.has_joined = joined(business_talk.id)
            BusinessTalkSerializer.new(business_talk, root: false)
          end

          response_pagination(serial_business_talk, @business_talks, 'Daftar Bincang Bisnis')
        end

        def show
          response_success('Data Bincang Bisnis', BusinessTalkSerializer.new(@business_talk))
        end

        def create_participant
          if @business_talk.can_participate? && @business_talk.event_not_outdated?
            participant = @business_talk.participants.build(user_id: current_mobile.id)
            if participant.save
              @business_talk.notifications.build(
                user_id: current_mobile.id,
                title: 'Pendaftaran Bincang Bisnis',
                message: 'Selamat, anda berhasil bergabung di Bincang Bisnis',
                action: 'join'
              ).save
              response_success('Pendaftaran Bincang Bisnis Berhasil', BusinessTalkSerializer.new(participant.event))
            else
              response_error('Pendaftaran Bincang Bisnis Gagal', participant.errors.full_messages)
            end
          else
            response_error('Pendaftaran Bincang Bisnis Gagal', 'Mohon Maaf, kuota Bincang Bisnis sudah penuh')
          end
        end

        private

        def joined(event_id)
          Participant.where(user_id: current_mobile.id, event_id: event_id).present?
        end

        def search_business_talk
          use_time_filter(column: 'event_on')

          @business_talks = if params[:search] && params[:search] != '{search}'
                              BusinessTalk.show.search(params[:search])
                            else
                              BusinessTalk.show
                            end

          @business_talks = @business_talks.ransack(params[:q]).result
        end

        def set_business_talk
          @business_talk = BusinessTalk.find(params[:id])
        rescue StandardError
          response_error('Eror Bincang Bisnis', 'Data tidak ditemukan')
        end
      end
    end
  end
end
