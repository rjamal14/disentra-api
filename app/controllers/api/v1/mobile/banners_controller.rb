# frozen_string_literal: true

module Api
  module V1
    module Mobile
      # BannersController controller for user
      class BannersController < BaseController
        before_action :check_type, only: :index

        def index
          @banners = @banners.order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

          return response_success('Daftar Banner', []) unless @banners.present?

          serial_banner = @banners.map { |banner| BannerSerializer.new(banner, root: false) }

          response_success('Daftar Banner', serial_banner)
        end

        private

        def check_type
          @banners = Banner.all

          return unless @banners.present?

          @banners = @banners.where(banner_type: params[:banner_type])
          @banners = @banners.ransack(params[:q]).result
        end
      end
    end
  end
end
