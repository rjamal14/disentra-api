# frozen_string_literal: true

module Api
  module V1
    module Mobile
      # VillagesController controller for user
      class VillagesController < BaseController
        def index
          @villages      = Village.ransack(params[:q]).result
          @villages      = @villages.order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
          serial_village = @villages.map { |village| VillageSerializer.new(village, root: false) }

          response_success('Daftar Kelurahan', serial_village)
        end
      end
    end
  end
end
