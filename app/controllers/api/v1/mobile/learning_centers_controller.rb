# frozen_string_literal: true

module Api
  module V1
    module Mobile
      # LearningCentersController controller for user
      class LearningCentersController < MobileController
        include TimeFilter

        before_action :search_learning_center, only: :index
        before_action :content_view_params, only: :content_view

        def index
          @learning_centers = @learning_centers.page(params[:page] || 1).per(params[:per_page] || 10)
                                               .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

          serial_learning_center = @learning_centers.map do |learning_center|
            LearningCenterSerializer.new(learning_center, root: false)
          end

          response_pagination(serial_learning_center, @learning_centers, 'Daftar Sentra Belajar')
        end

        def content_view
          content_view = ContentView.new(content_view_params.merge(user_id: current_mobile.id))

          return response_success('Lihat Content Berhasil, sudah pernah dilihat') if content_view.viewed?

          if content_view.save
            response_success('Lihat Content Berhasil')
          else
            response_error('Lihat Content Gagal', content_view.errors.full_messages)
          end
        end

        private

        def search_learning_center
          use_time_filter(column: 'created_at')

          @learning_centers = if params[:search].present? && params[:search] != '{search}'
                                LearningCenter.show.search(params[:search])
                              else
                                LearningCenter.show
                              end

          @learning_centers = @learning_centers.ransack(params[:q]).result
        end

        def content_view_params
          params.fetch(:content_view, {}).permit(:learning_center_id)
        end
      end
    end
  end
end
