# frozen_string_literal: true

module Api
  module V1
    module Mobile
      # DistrictsController controller for user
      class DistrictsController < BaseController
        def index
          @districts      = District.ransack(params[:q]).result
          @districts      = @districts.order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
          serial_district = @districts.map { |district| DistrictSerializer.new(district, root: false) }

          response_success('Daftar Kecamatan', serial_district)
        end
      end
    end
  end
end
