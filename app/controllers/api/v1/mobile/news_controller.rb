# frozen_string_literal: true

module Api
  module V1
    module Mobile
      # NewsController controller for user
      class NewsController < MobileController
        include TimeFilter

        before_action :search_news, only: :index
        before_action :set_news, only: :show

        def index
          @news_list = @news_list.page(params[:page] || 1).per(params[:per_page] || 10)
                                 .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

          serial_news = @news_list.map do |news|
            NewsSerializer.new(news, root: false)
          end

          response_pagination(serial_news, @news_list, 'Daftar Berita')
        end

        def show
          response_success('Detail Berita', NewsSerializer.new(@news))
        end

        private

        def search_news
          use_time_filter(column: 'published_at')

          @news_list = if params[:search].present? && params[:search] != '{search}'
                         News.search(params[:search])
                       else
                         News
                       end

          @news_list = @news_list.ransack(params[:q]).result
        end

        def set_news
          @news = News.find(params[:id])
        rescue StandardError
          response_error('Eror Berita', 'Data tidak ditemukan')
        end
      end
    end
  end
end
