# frozen_string_literal: true

module Api
  module V1
    module Mobile
      # ProvincesController controller for user
      class ProvincesController < BaseController
        def index
          @provinces      = Province.ransack(params[:q]).result
          @provinces      = @provinces.order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
          serial_province = @provinces.map { |province| ProvinceSerializer.new(province, root: false) }

          response_success('Daftar Provinsi', serial_province)
        end
      end
    end
  end
end
