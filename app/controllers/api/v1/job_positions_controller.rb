# frozen_string_literal: true

module Api
  module V1
    # JobPositionsController controller for user
    class JobPositionsController < AdminController
      before_action :set_job_position, only: %i[show update destroy]
      before_action :search_job_position, only: :index
      skip_authorize_resource only: :all_job_position

      def index
        job_positions = @job_position.page(params[:page] || 1).per(params[:per_page] || 10)
                                     .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_job_position = job_positions.map { |job_position| JobPositionSerializer.new(job_position, root: false) }

        response_pagination(serial_job_position, job_positions, 'Daftar Jabatan')
      end

      def all_job_position
        job_positions = JobPosition.order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_job_position = job_positions.map { |job_position| JobPositionSerializer.new(job_position, root: false) }

        response_success('Daftar Jabatan', serial_job_position)
      end

      def show
        response_success('Data Jabatan', JobPositionSerializer.new(@job_position))
      end

      def create
        job_position = JobPosition.new(job_position_params.merge(created_by_id: current_admin.id))

        if job_position.save
          response_success('Tambah Jabatan berhasil', JobPositionSerializer.new(job_position), 201)
        else
          response_error('Tambah Jabatan gagal', job_position.errors.full_messages)
        end
      end

      def update
        job_position_attribute = job_position_params.merge(updated_by_id: current_admin.id)
        if @job_position.update(job_position_attribute)
          response_success('Ubah Jabatan berhasil', JobPositionSerializer.new(@job_position))
        else
          response_error('Tambah Jabatan gagal', @job_position.errors.full_messages)
        end
      end

      def destroy
        return unless @job_position.destroy

        response_success('Hapus Jabatan berhasil')
        @job_position.update(deleted_by_id: current_admin.id)
      end

      private

      def search_job_position
        @job_position = if params[:search].present? && params[:search] != '{search}'
                          JobPosition.search(params[:search])
                        else
                          JobPosition
                        end
      end

      def set_job_position
        @job_position = JobPosition.find(params[:id])
      rescue StandardError
        response_error('Eror Jabatan', 'Data tidak ditemukan')
      end

      def job_position_params
        params.fetch(:job_position, {}).permit(:name)
      end
    end
  end
end
