# frozen_string_literal: true

module Api
  module V1
    # ProvincesController controller for user
    class ProvincesController < AdminController
      before_action :set_province, only: %i[show update destroy]
      before_action :search_province, only: :index

      def index
        @provinces      = @provinces.page(params[:page] || 1).per(params[:per_page] || 10)
                                    .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_province = @provinces.map { |province| ProvinceSerializer.new(province, root: false) }

        response_pagination(serial_province, @provinces, 'Daftar Provinsi')
      end

      def show
        response_success('Data Provinsi', ProvinceSerializer.new(@province))
      end

      def create
        province = Province.new(province_params.merge(created_by_id: current_admin.id))

        if province.save
          response_success('Tambah Provinsi berhasil', ProvinceSerializer.new(province), 201)
        else
          response_error('Tambah Provinsi gagal', province.errors.full_messages)
        end
      end

      def update
        province_attribute = province_params.merge(updated_by_id: current_admin.id)
        if @province.update(province_attribute)
          response_success('Ubah Provinsi berhasil', ProvinceSerializer.new(@province))
        else
          response_error('Ubah Provinsi gagal', @province.errors.full_messages)
        end
      end

      def destroy
        return unless @province.destroy

        response_success('Hapus Provinsi berhasil')
        @province.update(deleted_by_id: current_admin.id)
      end

      private

      def search_province
        @provinces = if params[:search].present? && params[:search] != '{search}'
                       Province.search(params[:search])
                     else
                       Province
                     end
        @provinces = @provinces.ransack(params[:q]).result
      end

      def set_province
        @province = Province.find(params[:id])
      rescue StandardError
        response_error('Eror Provinsi', 'Data tidak ditemukan')
      end

      def province_params
        params.fetch(:province, {}).permit(:name)
      end
    end
  end
end
