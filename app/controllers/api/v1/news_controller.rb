# frozen_string_literal: true

module Api
  module V1
    # NewsController controller for admin manage news
    class NewsController < AdminController
      before_action :set_news, only: %i[show update destroy]
      before_action :search_news, only: %i[index collect]

      def index
        @news_list = @news_list.page(params[:page] || 1).per(params[:per_page] || 10)
                               .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

        serial_news = @news_list.map do |news|
          NewsSerializer.new(news, root: false)
        end

        response_pagination(serial_news, @news_list, 'Daftar Berita')
      end

      def show
        response_success('Data Berita', NewsSerializer.new(@news))
      end

      def create
        news = News.new(news_params)

        if news.save
          response_success('Tambah Berita berhasil', NewsSerializer.new(news), 201)
        else
          response_error('Tambah Berita gagal', news.errors.full_messages)
        end
      end

      def update
        news_attribute = news_params
        if @news.update(news_attribute)
          response_success('Ubah Berita berhasil', NewsSerializer.new(@news))
        else
          response_error('Ubah Berita berhasil', @news.errors.full_messages)
        end
      end

      def destroy
        return unless @news.destroy

        response_success('Hapus Berita berhasil')
      end

      def collect
        crawl = SpiderService::NewsListService.process("#{ENV['BJB_NEWS_URL']}/page/#{params[:page] || 1}")
        @news_list = @news_list.page(params[:page] || 1).per(params[:per_page] || 10)
                               .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

        serial_news = @news_list.map do |news|
          NewsSerializer.new(news, root: false)
        end
        response_pagination({ crawl_results: crawl, news_list: serial_news }, @news_list, 'Daftar Berita & Hasil Crawl')
      end

      private

      def search_news
        @news_list = if params[:search].present? && params[:search] != '{search}'
                       News.search(params[:search])
                     else
                       News
                     end

        @news_list = @news_list.ransack(params[:q]).result
      end

      def set_news
        @news = News.find(params[:id])
      rescue StandardError
        response_error('Eror Berita', 'Data tidak ditemukan')
      end

      def news_params
        params.fetch(:news, {}).permit(:title, :body, :image, :published_at)
      end
    end
  end
end
