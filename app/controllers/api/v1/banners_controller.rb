# frozen_string_literal: true

module Api
  module V1
    # BannersController controller for user
    class BannersController < AdminController
      before_action :set_banner, only: %i[show update destroy]
      before_action :check_type, only: :index

      def index
        @banners = @banners.page(params[:page] || 1).per(params[:per_page] || 10)
                           .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

        serial_banner = @banners.map { |banner| BannerSerializer.new(banner, root: false) }

        response_pagination(serial_banner, @banners, 'Daftar Banner')
      end

      def show
        response_success('Data Banner', BannerSerializer.new(@banner))
      end

      def create
        banner = Banner.new(banner_params.merge(created_by_id: current_admin.id))

        if banner.save
          response_success('Tambah Banner berhasil', BannerSerializer.new(banner), 201)
        else
          response_error('Tambah Banner gagal', banner.errors.full_messages)
        end
      end

      def update
        banner_attribute = banner_params.merge(updated_by_id: current_admin.id)
        if @banner.update(banner_attribute)
          response_success('Ubah Banner berhasil', BannerSerializer.new(@banner))
        else
          response_error('Ubah Banner gagal', @banner.errors.full_messages)
        end
      end

      def destroy
        return unless @banner.destroy

        response_success('Hapus Banner berhasil')
        @banner.update(deleted_by_id: current_admin.id)
      end

      private

      def check_type
        @banners = if params[:banner_type].present? && params[:search] != '{search}'
                     Banner.where(banner_type: params[:banner_type])
                   else
                     Banner
                   end
        @banners = @banners.ransack(params[:q]).result
      end

      def set_banner
        @banner = Banner.find(params[:id])
      rescue StandardError
        response_error('Eror Banner', 'Data tidak ditemukan')
      end

      def banner_params
        params.fetch(:banner, {}).permit(:banner_type, :link, :image, :status)
      end
    end
  end
end
