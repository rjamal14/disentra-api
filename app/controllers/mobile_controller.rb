# frozen_string_literal: true

# Class of Mobile Record
class MobileController < ApplicationController
  before_action -> { doorkeeper_authorize! :mobile }
  before_action :current_mobile

  def current_mobile
    return if doorkeeper_token.nil?

    User.mobile.where(id: doorkeeper_token.resource_owner_id).last
  end
end
