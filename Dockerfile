FROM ruby:2.6.3-alpine AS build-env
ENV RAILS_ENV=production
ENV RACK_ENV=production
ENV RAILS_LOG_TO_STDOUT=true
ENV RAILS_ROOT=/app
ENV LANG=C.UTF-8
ENV GEM_HOME=/bundle
ENV BUNDLE_PATH=$GEM_HOME
ENV BUNDLE_APP_CONFIG=$BUNDLE_PATH
ENV BUNDLE_BIN=$BUNDLE_PATH/bin
ENV PATH=/app/bin:$BUNDLE_BIN:$PATH
ENV BUNDLER_VERSION=2.2.9
RUN apk add --update --no-cache \
      build-base \
      musl \
      curl \
      git \
      libstdc++ \
      libffi-dev \
      libc-dev \
      linux-headers \
      libgcrypt-dev \
      postgresql-dev \
      sqlite-dev \
      tzdata \
      yaml-dev \
      zlib-dev \
      wget \
      nodejs \
      imagemagick \
      && rm -rf /var/cache/apk/*
ARG DB_USERNAME
ARG DB_PASSWORD
ARG DB_HOST
ARG DB_PORT
ARG DB_NAME
ARG FULLHOST
ARG HOST
ARG SECRET_KEY_BASE
ARG MAILER_PORT
ARG MAILER_SENDER
ARG MAILER_USERNAME
ARG MAILER_HOST
ARG MAILER_ADDRESS
ARG MAILER_DOMAIN
ARG MAILER_PASSWORD
ARG ACCESS_KEY
ARG SECRET_KEY
ARG DO_REGION
ARG DO_BUCKET
ARG ASSET_HOST
ARG END_POINT
ARG DO_FOLDER
ARG WEB_HOST
ARG GOOGLE_CLIENT_ID
ARG GOOGLE_CLIENT_SECRET
ARG FCM_SERVER_KEY
ARG ROLLBAR_TOKEN
ARG BJB_NEWS_URL
ARG BJB_URL
ARG YOUTUBE_KEY
RUN mkdir api-areon
WORKDIR /api-areon
COPY Gemfile Gemfile.lock ./
RUN touch /api-areon/.env
RUN echo "DB_USERNAME=${DB_USERNAME}" >> /api-areon/.env
RUN echo "DB_PASSWORD=${DB_PASSWORD}" >> /api-areon/.env
RUN echo "DB_HOST=${DB_HOST}" >> /api-areon/.env
RUN echo "DB_PORT=${DB_PORT}" >> /api-areon/.env
RUN echo "DB_NAME=${DB_NAME}" >> /api-areon/.env
RUN echo "FULLHOST=${FULLHOST}" >> /api-areon/.env
RUN echo "WEB_HOST=${WEB_HOST}" >> /api-areon/.env
RUN echo "HOST=${HOST}" >> /api-areon/.env
RUN echo "SECRET_KEY_BASE=${SECRET_KEY_BASE}" >> /api-areon/.env
RUN echo "MAILER_PORT=${MAILER_PORT}" >> /api-areon/.env
RUN echo "MAILER_SENDER=${MAILER_SENDER}" >> /api-areon/.env
RUN echo "MAILER_USERNAME=${MAILER_USERNAME}" >> /api-areon/.env
RUN echo "MAILER_HOST=${MAILER_HOST}" >> /api-areon/.env
RUN echo "MAILER_ADDRESS=${MAILER_ADDRESS}" >> /api-areon/.env
RUN echo "MAILER_DOMAIN=${MAILER_DOMAIN}" >> /api-areon/.env
RUN echo "MAILER_PASSWORD=${MAILER_PASSWORD}" >> /api-areon/.env
RUN echo "ACCESS_KEY=${ACCESS_KEY}" >> /api-areon/.env
RUN echo "SECRET_KEY=${SECRET_KEY}" >> /api-areon/.env
RUN echo "DO_REGION=${DO_REGION}" >> /api-areon/.env
RUN echo "DO_BUCKET=${DO_BUCKET}" >> /api-areon/.env
RUN echo "ASSET_HOST=${ASSET_HOST}" >> /api-areon/.env
RUN echo "END_POINT=${END_POINT}" >> /api-areon/.env
RUN echo "DO_FOLDER=${DO_FOLDER}" >> /api-areon/.env
RUN echo "GOOGLE_CLIENT_ID=${GOOGLE_CLIENT_ID}" >> /api-areon/.env
RUN echo "GOOGLE_CLIENT_SECRET=${GOOGLE_CLIENT_SECRET}" >> /api-areon/.env
RUN echo "FCM_SERVER_KEY=${FCM_SERVER_KEY}" >> /api-areon/.env
RUN echo "ROLLBAR_TOKEN=${ROLLBAR_TOKEN}" >> /api-areon/.env
RUN echo "BJB_NEWS_URL=${BJB_NEWS_URL}" >> /api-areon/.env
RUN echo "BJB_URL=${BJB_URL}" >> /api-areon/.env
RUN echo "YOUTUBE_KEY=${YOUTUBE_KEY}" >> /api-areon/.env
RUN gem install bundler -v 2.2.9
RUN bundle install -j "$(getconf _NPROCESSORS_ONLN)"
RUN rm -rf $BUNDLE_PATH/cache/*.gem \
  && find $BUNDLE_PATH/gems/ -name "*.c" -delete \
  && find $BUNDLE_PATH/gems/ -name "*.o" -delete 
RUN rm -rf /bundle/ruby/2.6.0/cache/*.gem
COPY . ./
RUN chmod +x entrypoints/docker-entrypoint.sh
ENTRYPOINT ["./entrypoints/docker-entrypoint.sh"]