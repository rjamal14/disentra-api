# frozen_string_literal: true

require 'test_helper'

module ApplicationCable
  # application cable for testing class
  class ConnectionTest < ActionCable::Connection::TestCase
  end
end
